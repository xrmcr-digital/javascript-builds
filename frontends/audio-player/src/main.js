import Vue from 'vue';
import App from './App.vue';

Vue.config.productionTip = false;

new Vue({
  data: {
    file: 'https://deploy-preview-79--xenodochial-hamilton-7d579a.netlify.com/assets/audio/2018-08-3-BBC-Radio-4-Jim-Naughty.mp3',
    name: 'Jim Naughtie on BBC Radio 4',
  },
  render: h => h(App),
}).$mount('#app');
