module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'max-len': 'off', // Due to SVGs
    'no-tabs': 'off', // Due to SVGs
    'no-mixed-spaces-and-tabs': 'off', // Due to SVGs
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
};
