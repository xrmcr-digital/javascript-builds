module.exports = {
  css: {
    modules: true,
    extract: false,
  },
  filenameHashing: false,
  outputDir: '../../dist/audio-player',
  chainWebpack: (config) => {
    config.optimization.delete('splitChunks');
  },
};
