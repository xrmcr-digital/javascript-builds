# audio-player

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# Styling
Styling has been built to allow CSS Variable overrides
```css
.audio-player-app {}
    --primary: #FDC916;
    --secondary: #FFF;
    --tertiary: #000;

    --background-color: var(--tertiary);
    --button-color: var(--secondary);
    --button-color-active: var(--primary);

    --font-color: var(--primary);
    --font-family: Arial;
    --font-base-size: 16px;
}
```
