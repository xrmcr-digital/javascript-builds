import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    pages: [],
  },
  mutations: {
    addPage(store, page) {
      // eslint-disable-next-line no-param-reassign
      store.pages = [...store.pages, page];
    },
  },
  actions: {
    addPage({ commit }, {
      title,
      permalink,
      excerpt,
      content,
    }) {
      const newPage = {
        title,
        permalink,
        excerpt,
        content,
      };
      commit('addPage', newPage);
    },
  },
});
