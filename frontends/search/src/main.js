import Vue from 'vue';
import App from './App.vue';

Vue.config.productionTip = false;

new Vue({
  data: {
    label: 'Search',
    api: '/api/search-data.json',
  },
  render: h => h(App),
}).$mount('#app');
