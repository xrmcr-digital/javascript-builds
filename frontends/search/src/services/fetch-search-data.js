async function fetchSearchData(dataUrl) {
  return fetch(dataUrl)
    .then(data => data.json())
    .then(data => data.filter(d => d.title));
}

export default fetchSearchData;
