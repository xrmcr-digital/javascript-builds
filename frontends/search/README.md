# search

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### CSS Variables

```css
    --search-border-bottom-color: #{$dark-gray};
    --search-label-color: #{$brand-primary};
    --search-label-font-family: Fucxed Caps;
    --search-label-font-size: 20px;
    --search-input-font-family: Crimson Text;
    --search-input-font-size: 20px;
    --search-active-border-bottom-color: #{$brand-contrast};
    --search-active-label-color: #{$dark-gray};
    --search-active-label-font-size: 16px;

    --search-results-header-font-family: Fucxed Caps;
    --search-results-header-font-size: 26px;
    --search-result-font-family: Fucxed Caps;
    --search-result-font-size: 20x;
    --search-result-border-color: #{$brand-primary};
    --search-excerpt-font-family: Crimson Text;
    --search-excerpt-color: #{$brand-primary};
```
