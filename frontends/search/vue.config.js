module.exports = {
  css: {
    modules: true,
    extract: false,
  },
  productionSourceMap: false,
  filenameHashing: false,
  chainWebpack: (config) => {
    config.optimization.delete('splitChunks');
  },
  devServer: {
    proxy: {
      '^/api': {
        target: 'http://localhost:4000',
        ws: true,
        changeOrigin: true,
      },
    },
  },
};
