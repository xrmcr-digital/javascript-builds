module.exports = {
  css: {
    modules: true,
    extract: false,
  },
  productionSourceMap: false,
  filenameHashing: false,
  chainWebpack: (config) => {
    config.optimization.delete('splitChunks');
  },
};
