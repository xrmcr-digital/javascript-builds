# find-your-nearest

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# How to embed

```html
<script src="https://unpkg.com/vue"></script>
<script src="./xrmcrFindYourNearest.umd.js"></script>

<div id="find-your-nearest">
  <find-your-nearest></find-your-nearest>
</div>

<script>
new Vue({
  components: {
    'find-your-nearest': findYourNearest
  },
  data: {
      groups: 
          {
              'whalley-range': { 
                  name: "Whalley Range",
                  day: "Sat",
                  time: "10:30",
                  location: "Coffee Cranks Co-op Cafe, Alexandra Park, Demesne Road, Manchester, M16 8PJ",
                  coordinates: "53.449891, -2.248633"
              }
          },
          ...
  }
}).$mount('#find-your-nearest');
</script>

```
