import Vue from 'vue';
import App from './App.vue';
import groups from './stubs/groups.json';

Vue.config.productionTip = false;

new Vue({
  data: {
    groups,
    title: 'Find your local group',
  },
  render: h => h(App),
}).$mount('#app');
