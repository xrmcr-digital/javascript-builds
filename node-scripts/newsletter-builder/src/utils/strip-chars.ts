function stripChars(content: string): string {
  return content
    .replace(/[^a-z0-9\-"' ]/gmi, '')
    .replace(/\s+/g, ' ')
    .trim();
}

export default stripChars;
