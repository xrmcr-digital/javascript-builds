function createUrl(domain: string, date: Date, slug: string): string {
  const rawYear = +date.getFullYear();
  const rawMonth = +date.getMonth() + 1; // +1 because in JS, months are Zero indexed
  const rawDay = +date.getDate();

  const year = `${rawYear}`;
  const month = (rawMonth < 10) ? `0${rawMonth}` : `${rawMonth}`;
  const day = (rawDay < 10) ? `0${rawDay}` : `${rawDay}`;

  return [domain, year, month, day, slug].join('/');
}

export default createUrl;
