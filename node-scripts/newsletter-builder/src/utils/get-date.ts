const pattern = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;

function getDate(file: string) {
  const [_, date] = file.match(pattern);
  return new Date(date);
}

export default getDate;
