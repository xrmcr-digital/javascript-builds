import fm from 'front-matter';

export interface FrontMatter {
  layout: string;
  author: string;
  title: string;
  author_profile: boolean;
  categories: string[];
  tags: string[];
  share: boolean;
  excerpt: string;
  permalink: string;
}

function getFrontMatter(md: string): FrontMatter {
  return fm<FrontMatter>(md).attributes as FrontMatter;
}

export default getFrontMatter;
