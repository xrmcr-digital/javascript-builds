import moment from 'moment';

function getFriendlyDate(date: Date): string {
  return moment(date).format('dddd, MMMM Do YYYY');
}

export default getFriendlyDate;
