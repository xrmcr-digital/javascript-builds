const pattern = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))-(.*)/;

function getSlug(file: string) {
  const [_, date, month, day, slug] = file.match(pattern);
  return slug
    .replace('.md', '');
}

export default getSlug;
