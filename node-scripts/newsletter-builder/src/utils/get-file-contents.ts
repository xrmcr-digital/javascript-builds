import fs from 'fs';

function getFileContents(path: string, file: string) {
  return fs.readFileSync(`${path}/${file}`).toString();
}

export default getFileContents;
