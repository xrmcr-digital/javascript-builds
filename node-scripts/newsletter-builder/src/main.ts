import fs from 'fs';
import _ from 'lodash';
import args from 'args';
import juice from 'juice';

import template from './template.html';
import MdParser from './MdParser';

args
  .option('path', 'Path to Jekyll blogs', '_posts')
  .option('output', 'Name of HTML File', 'newsletter-preview.html')
  .option('frequency', 'Number of days posts to pull', 7);

const flags = args.parse(process.argv);

const { path, output } = flags;

const fromDate = new Date();
fromDate.setDate(fromDate.getDate() - (+flags.frequency));
fromDate.setHours(0, 0, 0);

const pathContents = fs.readdirSync(path);

const articles = pathContents
  .filter(file => file.includes('.md'))
  .map(file => new MdParser(path, file))
  .filter(article => article.excerpt)
  .filter(article => article.date > fromDate)
  .sort((a, b) => a.date < b.date ? 1 : -1)
  .map(({ url, title, excerpt, friendlyDate }) => ({
    url,
    title,
    excerpt,
    friendlyDate,
  }));

if (articles.length === 0) {
  console.log('Cannot generate email: No Articles');
  process.exit(0);
}

const hero = {
  hero: true,
  url: 'https://xrmcr.org/northern-rebellion',
  title: 'Northern Rebellion',
  friendlyDate: '30 Aug - 2 Sept 2019',
  excerpt: [
    'People in the North of England are rising up for the ecological emergency',
    '- join us to Act Now!',
  ].join(' '),
  cta: 'Join the Rebellion',
  image: {
    src: 'https://xrmcr.org/assets/images/nrb/yellow-boat.jpg',
    alt: 'Yellow boat with \'Planet Before Profit\' written on the side of it',
  },
};

const templateFn = _.template(template);
const title = 'XRMCR Newsletter';
const outputTemplate = templateFn({ title, articles: [hero, ...articles] });

const inlinedCssTemplate = juice(outputTemplate);

fs.writeFileSync(output, inlinedCssTemplate);
