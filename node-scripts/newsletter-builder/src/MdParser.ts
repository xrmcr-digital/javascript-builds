import getFileContents from './utils/get-file-contents';
import getFrontMatter, { FrontMatter } from './utils/get-front-matter';
import getDate from './utils/get-date';
import getSlug from './utils/get-slug';
import stripChars from './utils/strip-chars';
import createUrl from './utils/create-url';
import getFriendlyDate from './utils/get-friendly-date';

const domain = 'https://xrmcr.org';

class MdParser {

  date: Date = getDate(this.filename);
  friendlyDate: string = getFriendlyDate(this.date);
  slug: string = getSlug(this.filename);
  mdFile: string = getFileContents(this.path, this.filename);
  frontMatter: FrontMatter = getFrontMatter(this.mdFile);
  url: string = this.frontMatter.permalink
    ? `${domain}${this.frontMatter.permalink}`
    : createUrl(domain, this.date, this.slug);

  title = stripChars(this.frontMatter.title);
  excerpt = this.frontMatter.excerpt ? stripChars(this.frontMatter.excerpt) : null;

  constructor(private path: string, private filename: string) {
  }

}

export default MdParser;
