const path = require('path');
const webpack = require('webpack');

const babel = {
    test: /\.m?js$/,
    exclude: path.resolve(__dirname, 'node_modules'),
    use: {
        loader: 'babel-loader',
        options: {
            presets: [
                '@babel/preset-env',
            ]
        }
    }
};

const html = {
    test: /\.(html)$/,
    use: {
        loader: 'html-loader',
        options: {
        }
    }
};

const typescript = {
    test: /\.ts(x?)$/,
    exclude: path.resolve(__dirname, 'node_modules'),
    use: [
        {loader: "ts-loader"}
    ],
};

const tslint = {
    test: /\.ts$/,
    enforce: 'pre',
    use: [
        {
            loader: 'tslint-loader',
            options: {
            }
        }
    ]
};

const sourcemaps = {
    enforce: "pre",
    test: /\.js$/,
    loader: "source-map-loader"
};

module.exports = (env, argv, production = argv.mode === 'production') => ({
    target: "node",

    entry: {
        index: './src/main.ts'
    },

    plugins: [
        new webpack.NormalModuleReplacementPlugin(/^any-promise$/, 'bluebird'),
    ],

    resolve: {
        extensions: [".html", ".js", ".ts"]
    },

    module: {
        rules: [
            babel,
            tslint,
            typescript,
            sourcemaps,
            html,
        ]
    },

    output: {
        filename: 'main.js',
        path: production
            ? path.resolve(__dirname, '../../dist/newsletter-builder')
            : path.resolve(__dirname, 'dist'),
    }

});
