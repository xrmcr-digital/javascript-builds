---
layout: single
author: grahame
title: Jim Naughtie on BBC Radio 4
author_profile: false
categories: [Climate Change, Weather]
tags: []
share: true
excerpt: "Jim Naughtie Tells the Truth and makes the case to Act Now in this excellent but chilling piece."
---

## Climate change and weather patterns
In a recent interview on Radio 4, Jim Naughtie Tells the Truth and makes the case to Act Now in this excellent but
chilling piece.

{% include audio-player.html file="/assets/audio/2018-08-3-BBC-Radio-4-Jim-Naughty.mp3" title="Jim Naughtie on BBC Radio 4" %}

<small>Excerpt taken from [BBC Radio 4](https://www.bbc.co.uk/programmes/m00079zs){:target="_blank"} (Requires sign in).</small>

### Transcript

The weather is becoming a much more extreme business.
The waters are out in places in northern England, passing through the vale of York yesterday you could see sodden fields,
pools of water with sad crops sticking up from the surface -- a landscape of little lakes.

In Derbyshire they were still trying to shore up the reservoir that was threatening to engulf Whaley Bridge, pumping it's
contents out to get the water down.

In places where flooding has become so common -- like East Yorkshire, Cumbria, parts of Wales, the Somerset levels --
They follow these stories wearily with their attendant cliches of tight nit communities, mass mopping up operations
and the Dunkirk spirit alive in the Church hall.

Because it's not only the heat that's rising, as we were told at the end of one of the hottest Julys across Europe, 
it's the water:

The Trent and the Wye in Derbyshire, the Mersey and its tributary the river Bollin that runs for 30 miles from 
the edge of the Peak District were all rising. For much of yesterday there were more than a dozen flood warnings in place.

And as people whose homes have been effected well know, long after the news cameras have left they may still be trying
to sort out their lives because these events dont just pass away like a spectacular flash flood, they leave their mark 
for a very long time, and you know that stubbornly one day the water will be back.

We're told to expect extreme events more often, a friend living in the West Highlands tells me of 4 inch rainfalls in a
day or two becoming commonplace -- they were once a rarity.

> Spending on flood defences has to rise to at least £1bn a year

The environment agency says spending on flood defences has to rise to at least £1bn a year, and dont even talk to people
whose homes have been build on a flood plain about property values or insurance. The agency says the risk of inundation
in some areas have become so great that people will simply have to move.

And just as changes to the climate have unbalanced the settled order around the world: agricultural production, changes
to landscape and forests are likely to produce patterns of mass migration.
Here we'll have to get used to the idea, that the texture of our own intimate natural world, you know, gardens on high
moors and on sea shores will alter.

Behind the headlines from the flooded village and towns, there's a deeper story of people who are thinking about how to
adapt, trying to protect peat plans that absorb the water, planting more trees who's roots make the land more permeable
and help to let the water run away, building houses that can cope.

They see the signs everwhere, speaking of new patterns for example of fish migration because their spawning grounds are
being washed out, crop rotations that will have to change, different kinds of land use, of a world that seems on the turn.

Now all this isn't an academic debate about what's happening to the climate or why, which could easily reach hysteria 
levels between scientists and non-believers. It's practical, the here and now, the people who work on the land don't 
have to be told that the 10 hottest years in the history of our recorded temperatures have all been in this century, 
because they see the evidence everyday, and feel it on fields and in rivers on high hills, in places where the 
coastline is always on the move, and of course wildlife knows it better than we do -- they send their own signals.
 
 Weather patterns and habitats are always changing, it's the speed that's new.
 
 It's hard to think of the Derbyshire reservoir without sensing a metaphor for this particular season of floods. A 
 provider of water that cant cope with the flow. The public source of the simplest and most necessary commodity of all
 that's become a threat to life. The impregnable dam that might burst.
 
 We always seen to be wanting rain, or wanting it to stop. Travelling under a laden sky yesterday, there came a moment
 just north of York where the rain started again. Not a downpour, just a grey drizzle, so maybe people will get some
 relief in the next few days. But the waters always find their way back, and surely they bring a message on every tide.
