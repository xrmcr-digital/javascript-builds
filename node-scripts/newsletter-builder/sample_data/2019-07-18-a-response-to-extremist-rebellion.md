---
layout: single
author: grahame
author_profile: true
title: "Response to Richard Walton and the report 'Extremism Rebellion' for the Policy Group"
categories: [letter]
share: true
excerpt: "We all agree the existential threat and we all agree on the need to act. But apart from a few core messages and demands there is no central authority telling us what to think or what political structure the solution will look like."
---

## What I love about XR is its inclusiveness

What I love about XR is its inclusiveness. As long as I follow the basic principles I'm in and I'm welcomed. 

We all agree the existential threat and we all agree on the need to act. But apart from a few core messages and demands
there is no central authority telling us what to think or what political structure the solution will look like. 

The idea that Roger and his cabal is secretly manipulating me - well that is a conspiracy theory if every I heard one. 

XR has no leader, it is non-hierarchical; it's structures are fluid, they form and then dissolve and new ones form.
In this sense it is anarchist organisation. I am definitely not an anarchist, but I support his structure. It works.

I was talking to someone from Friends of the Earth at The Time is Now mass lobby of MPs. Now FoE is a fairly flat 
organisation but still he has a line to go up (as they say in business jargon - 'ask your line' or 'ask your line 
manager'). He was surprised at the freedom to act that XR encourages. 

I know people who won't join XR because they think it is 'anarchic'. And people who think it is a call for a 'socialist 
revolution', and others who think it is for 'middle class hippies'. 

> "When we act on our climate emergency we will see where the solutions take us."

What I see on the inside is a group of very varied people united by only one thing. There is a massive clue in the name
-- Extinction Rebellion. My view is, when we act on our climate emergency we will see where the solutions take us. 

I think what we will see is that all the easier options went south years ago but I hope I am wrong. The people who 
founded XR think they know where the political solution will lie - but that is just their opinion. Perhaps it will be
a government of national unity like a war cabinet. Perhaps it is the collapse of capitalism. There are lots of ways 
it might go. I have my middle class centre left democratic solutions too. But, the point is: when we address the 
problem the consequences for our system will be clear and we have to live them (that rational view could come from 
any political position).

To those criticising XR for being a radical anti-capitalist movement I simply ask - well what is your solution to the 
impending global catastrophe? More police powers?

One thing that you can definitely say is nothing is hidden in XR. It's all out there. You don't need to be Bellingcat 
to know who we are or what we do. It is not a secretive organisation - it's a mass movement.

The criticisms of XR that it threatens the state by challenging the idea of perpetual growth are weird. The idea that 
we can't grow perpetually is an entirely mainstream view. The report "The Limits To Growth" commissioned by the Club of
Rome made this case back in 1972. The idea that this is a radical revolutionary position is ridiculous.

XR is not threatening the stability of society by trying to prevent its collapse. It is not XR that is looking to
bring down a civil society utterly dependent on the natural world. It is the failure of XR that will do that.

XR are critical of the anarchy of free market capitalism — that everybody does what they want and the invisible hand 
of the market sorts it out?

Insider trading regulations, minimum wages, health and safety, sick pay, maternity leave, redistribution, even the 
environment - a million things that temper exploitation. But in one area - the most important - they might as well 
be doing nothing. There is obviously no adequate protection of our environment. If there were, we wouldn't be where
we are now.

I fervently want XR to succeed and I am a centre left democrat. I used to work for an oil company. The idea that the 
supporters of XR don't know what they are doing is tosh. Some things are just too important. 

I support non violent-law breaking and arrest. 

For goodness sake. 


  
