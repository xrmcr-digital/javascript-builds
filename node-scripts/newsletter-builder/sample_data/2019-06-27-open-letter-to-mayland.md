---
layout: single
author: xr_trafford
title: An open letter to Mr Mayland at Albany Academy
author_profile: true
categories: [letter]
tags: [school, climate, protest, emergency, youth]
share: true
excerpt: Hannah Stanton of XR Trafford writes an open letter to Mr Mayland who is responsible for excluding three students from their school prom after they stood up for the world's climate emergency at the recent climate strike in Manchester.
---

## Dear Mr Mayland,

I’m writing to express my shock and concern at learning that three of your students who attended the recent climate strike in Manchester on 24th May have been banned from attending their school prom as a result.

How can this decision possibly fit with the declaration on the main page of your school website that Albany Academy “develops young people as confident individuals who are aware of their role and responsibilities within society”?

## The UK government has declared a climate emergency -

and yet it pursues policies (fracking, Cumbria’s new coal mine, removal of subsidies for renewable energy etc) that are in direct opposition to this declaration and threaten the wellbeing of us all.

Faced with this, what are young people to do? They cannot yet legally vote. Environmental campaigners have been sending emails and signing petitions for decades, with very little impact. This is the most urgent situation we have ever faced.

## Young people will pay the price for the luxurious lifestyles that previous generations (including ours) have enjoyed.

Climate change will affect everything in their future - their health, housing, employment, travel and diet choices. They absolutely have the right to (peacefully and democratically) demand climate action and so far the media and the government have proved they will only listen to young people’s voices when they are expressed through mass actions such as the Fridays for Future strikes.

Personally I would be extremely proud that my school was producing students who understand that their “role and responsibility within society” requires them to make their voice heard in defence of the Earth, the thousands of species at risk, the millions in developing countries who are currently battling the effects of climate change and the security and wellbeing of future generations.

## What lesson will the students take from their punishment?

That the generation who have benefited from the comfortable lifestyles which have caused the climate crisis will leave young people to fight for a better future alone? That they have to choose between being engaged citizens and being allowed the same enjoyments as their less conscientious classmates?

## I sincerely hope that you will reconsider your decision.

We need to throw our weight behind those young people who are passionate about fighting for the Earth. They are the hope for all of our futures.

Regards,

Hannah Stanton, XR Trafford
