---
layout: single
author: xr_trafford
title: XR Trafford raise awareness on air-pollution-related deaths on Clean Air Day
author_profile: true
categories: [action]
tags: [clean air day, trafford, pollution]
share: true
---

Today, members of Extinction Rebellion Trafford and Extinction Rebellion Manchester carried out an action on the A56 during the morning rush hour to highlight Clean Air Day.

We targetted the northbound carriageway at the junction with Dane Road, and handed out flyers and handmade windmills to motorists while they were stationary at traffic lights.

Our aim of the action was to raise awareness of the 1,200 air-pollution-related deaths a year within Greater Manchester and to encourage people to express their views via the GMCA Clean Air Plan consultation (which closes on 30th June 2019).

<figure class="half">
    <a href="{{ site.url }}{{ site.baseurl }}/assets/images/clean_air_day_2019/clean_air_day_flyer_1.jpg"><img src="{{ site.url }}{{ site.baseurl }}/assets/images/clean_air_day_2019/clean_air_day_flyer_1.jpg" alt="Person holding Extinction Rebellion poster reading Temporary Disruption, Permanent Change"></a>
    <a href="{{ site.url }}{{ site.baseurl }}/assets/images/clean_air_day_2019/clean_air_day_flag.jpg"><img src="{{ site.url }}{{ site.baseurl }}/assets/images/clean_air_day_2019/clean_air_day_flag.jpg" alt="Person holding Act Now flyers whilst holding an Extinction Rebellion white flag,"></a>
</figure>

The flyers also carried information about our demand for net zero carbon emissions by 2025.

XR member Hannah Stanton said the action was a success.

> Most of the drivers we approached were willing to wind down their windows and take the leaflets and we handed out over four hundred.

> A banner asking people to “Honk if you want clean air” attracted a lot of loud support from drivers, including lorries, vans and buses. It was fantastic to see the people of Trafford showing their support for action on clean air.

> We need much better public transport and walking and cycling infrastructure to enable people to leave their cars at home.

<figure class="half">
    <a href="{{ site.url }}{{ site.baseurl }}/assets/images/clean_air_day_2019/clean_air_day_act_now.jpg"><img src="{{ site.url }}{{ site.baseurl }}/assets/images/clean_air_day_2019/clean_air_day_act_now.jpg" alt="Act now protests disrupting traffic"></a>
    <a href="{{ site.url }}{{ site.baseurl }}/assets/images/clean_air_day_2019/clean_air_day_honk.jpg"><img src="{{ site.url }}{{ site.baseurl }}/assets/images/clean_air_day_2019/clean_air_day_honk.jpg" alt="Lady holding banner stating: honk if you want clean air"></a>
</figure>
