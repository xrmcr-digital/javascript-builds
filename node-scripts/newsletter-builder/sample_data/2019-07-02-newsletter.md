---
layout: single
author: default
author_profile: true
title: "XRMCR Newsletter 12: Beyond Politics"
categories: [newsletter]
tags: []
share: true
---

**The rebellion gathered pace as Glastonbury rebels marched to XR’s beat and Europe sweltered under an unprecedented heat wave. In Manchester, the momentum continued as we head towards the Northern Rebellion and an autumn of international action.**

This week saw the further improvements to our website. The [local groups](/local-groups "XRMCR Local Groups") page went live and is testament to the success of our decentralisation efforts – we now have 16 groups!

From now on, all enquires and offers of help for XRMCR and the Northern Rebellion should be sent to the relevant working group through our [contact page](/contact "Contact XRMCR").

### Can you host for the Northern Rebellion?

We’re setting up our own Rebel Airbnb! If you can offer activists from outside Manchester a bed, sofa or even a space to pitch a tent in your garden, email [northernxr@gmail.com](mailto:northernxr@gmail.com "Email northernxr@gmail.com"). We’re also looking for volunteers to steward. Please email the same address with the subject ‘steward’. Anyone with free access to a PA system, large sound system, battery or solar generator, staging, curtainsider truck, megaphones, high –viz, festival tent/gazebo, barriers/railings and catering equipment is asked to get in touch with the subject line ‘kit’.

One event not to miss this week is [our talk on citizens’ assemblies](https://www.facebook.com/events/2439478482961904/ "Citizens' assemblies talk"). We welcome Director of the [Sortition Foundation](https://www.sortitionfoundation.org/ "Sortition Foundation") Brett Honnig to discuss this innovative form of participatory democracy. Brett, who has experience running assemblies across Europe, will share his insight and facilitate an audience discussion. Free tickets for the event taking place at the Methodist Central Building can be obtained [here](https://www.eventbrite.co.uk/e/citizens-assemblies-what-are-they-and-why-we-need-them-tickets-64361907197 "Tickets").

A citizens’ assembly on our climate and environmental crisis is XR’s third demand and the theme of this week’s newsletter. Keep reading on for further information, upcoming events and a climate news roundup.

## A HIVE OF RECENT ACTIVITY

### XR Chorlton Launch Event

![XR Chorlton Launch]({{ site.url }}{{ site.baseurl }}/assets/images/newsletters/20190702/xr-chorlton-launch.jpg "XR Chorlton Launch")

**XR Member Dave reports:**

"[Extinction Rebellion Chorlton](https://www.facebook.com/groups/2412886828763182/ "Extinction Rebellion Chorlton") had a successful launch event last Friday at the Irish club which was attended by around 65 people. The event had a mood of love, rage, and hope about it. Speakers included local secondary school students who inspired all with their eloquence and determination.

A member who had been present at the rebellion in London spoke movingly of her reasons to be part of XR and a local teacher was also on the panel. Most of the time was set aside for questions from the audience, excellently facilitated by a local Chorltonite, and questions were engaging and answers informative. Testament to its success, many people stayed afterwards to have a drink and discuss their grief, hope, rage, and cautious optimism for the future. Two city councillors also attended the event, sharing their support for XR’s aims, and were asked robust questions by the audience (a student’s dad!) about how MCC will do its part. They did not disappoint.

It was an inspiring evening overall, with many more XR Chorlton members as a result. XR is clearly playing an important role in mobilising people to pressurise global governments for profound and immediate systemic change.

The launch will be followed with the **‘Heading for Extinction talk’ on the 17th of July at the Yoga Café from 7:30pm.**”

### XR Discussion: Talking about talking: whiteness, racism and decolonisation workshop

XRMCR held its first diversity workshop last Saturday with XR diversity facilitators and one of our BAME coordinators. The discussion addressed topics such as navigating difficult conversations of racism and white systemic power. XR wanted to understand how to appeal to a broader range of people and to give people of colour more confidence in their white co-organisers. A good quote to take away from the event was "undoing white privilege is not like pulling a tooth. It's like brushing your teeth twice a day, every day, for the rest of your life".

## BEYOND POLITICS – THE THIRD DEMAND

Two weeks ago, the six House of Commons select committees [announced its plans to hold a citizens’ assembly on the climate emergency](https://www.parliament.uk/business/committees/committees-a-z/commons-select/business-energy-industrial-strategy/news-parliament-2017/climate-change-and-net-zero-chairs-comments-17-19/ "Climate Emergency Announcement"). Extinction Rebellion has called this a [‘half arsed attempt’](https://rebellion.earth/2019/06/20/response-to-select-committees-announcing-a-citizens-assembly-have-we-achieved-our-third-demand/ "Half arsed attempt").

So what is XR’s third demand of a citizens’ assembly and what are the problems with the government’s proposal?

A citizens’ assembly is a group convened to deliberate on important issues with political advisory power. Its participants are drawn on the basis of ‘sortition’, much in the same way we select juries. Members are randomly selected from a pool of candidates and quotas are used to ensure different genders, ethnicities, class, etc. are all represented. Citizens’ assemblies aim to avoid factionalism and combat the elitism characteristic of today’s political system – something which is undoubtedly one of the root causes of our government’s inertia on the climate crisis. The assembly is presented with scientific evidence and expert opinion to develop responses based on rational and reasoned dialogue.

Citizens’ assemblies have been proven to work well in Canada, Australia, Belgium, Poland, and [recently in Ireland](https://www.theguardian.com/uk-news/2019/jan/17/power-to-the-people-could-a-citizens-assembly-solve-the-brexit-crisis "Recently in Ireland"). They have the ability to give politicians an indisputable mandate to do what is in the public’s interest.

### So why has XR criticised the government’s recent proposed citizens’ assembly?

Well, firstly the suggestion wouldn’t have any legislative powers and the government would be under no obligation to listen to the assembly’s conclusions. Secondly, the proposal does not appear to be independent from the government’s own self-imposed target to reach carbon zero by 2050. It is imperative that any assembly is allowed to operate independently, based on facts and expert opinion.

The third demand of a citizens’ assembly is a key element to our organisation. Criticism has been levelled at XR that we disrupt and protest without proposing solutions. This could not be further from the truth. We need your participation to develop this revolutionary democratic system and find solutions to the climate emergency. Come along to our [citizens’ assemblies event on Friday](https://www.facebook.com/events/2439478482961904/ "Friday event") to find out more information and add to the discussion.

## BEE IN YOUR BONNET

**Last week we gave Graheme and Elliot a chance to reply to Paul Harnett’s Cap and Dividend suggestions. Here’s Paul’s response:**

Firstly there is no tax beyond the cap. The beauty of the cap is that we control production directly rather than depend on a tax to hopefully control production to the extent that we want. If it was taxed to the right extent then the outcome would be pretty much the same. The revenue comes from selling licenses to extract carbon. The licenses are limited up to the value of the cap. Maybe think in terms of how UK sells 4g mobile licenses (reverse Dutch auction) - this would generate huge amounts of money.

1. Governments would be important in reaching an international agreement with regards to the cap - once that is in place it is up to governments to decide what royalties they want from carbon extraction companies still. However, it would be the companies that bid for the licenses - if those companies happened to be nationalised - so be it. If governments bought those licenses with a view to passing them onto companies then that's fine as well (though less efficient as what should really happen is that the companies should gravitate towards carbon that is easiest to extract). BTW there is no reason why a ban on fracking could not accompany this agreement.
1. For sure oil prices would go up but production would be capped to what we need (no doubt revised over time). As oil prices go up then that signals major shifts in incentives for the world's economy - renewables become even more profitable, so too with insulation, public transport, and other aspects of a transformative economy e.g. shop local, work local and cycle.
1. There is no carbon tax suggested.

As for your view, that's fine and dandy but for sure the carbon extracting companies will drive a bus through the agreements (BTW many of them have already accounted for a future tax - and are promoting the idea of a tax which they know will be imperfect). If countries agree to reduce carbon emissions then we are back to the sprinkler and reliant on armies of carbon accountants ensuring that every product has its carbon footprint included. Witness the UK at present which claims it has reduced its carbon footprint when in fact imports have a large carbon footprint which is not taken into account - similar with air travel. This method is both complicated and also open to abuse by every country not mention the multinationals!
Regarding deforestation then we can propose other means to address this. These may include a carbon footprint - or we could simply ban deforestation internationally.

The real weakness (I think) in cap and share is that we are a long way from international agreements that would enable this. The UK and USA (plus allies) have been torpedoing reasonable demands for international tax agreements for years. However, I think it is important to spell out the clearest way to achieve a reduction in carbon emissions and this is it.
On another note, in many countries a Green New Deal has become "opposed" to a carbon tax largely because the carbon tax has been pushed by oil companies. I think we need to cut through this and agree that both are useful to achieve our ends - my only addition is that a cap is better than a tax as we are then sure to what extent emissions are being reduced. The current environment is that fossil fuels are subsidised to the tune of \$5.2 Trillion internationally and carbon extractors have high positions in many of the world's major economies.

I'd be happy to see even Paris happen in this environment - it would be a step in the right direction but I believe cap and share would be the gold standard to be aimed for.

**XR member Pete had something to say about Cap and Share, as well as Emil’s analysis of the first demand in last week’s newsletter:**

> This is great stuff. However ‘Tell the Truth’ is not so much about climate change itself (or indeed species extinction - not mentioned by Emil) as about the causes, namely the extraction and burning of fossil fuels, industrialised farming and so on, and about government responsibility for these causes.

> By the way, it’s generally known that cap and share is inadequate - the debate has moved on and is now focused on the possibility of a Green New Deal.

**If you’ve got a bee in your bonnet, send your thoughts to [hello@xrmcr.org](mailto:hello@xrmcr.org "Email XRMCR") with the subject, ‘newsletter’.**

## THE CLIMATE BUZZ

XR dominated Glastonbury this weekend with the Guardian going so far as asking [‘is this the greenest Glastonbury yet?](https://www.theguardian.com/music/2019/jun/30/greenest-glastonbury-david-attenborough-climate-crisis-plastic "Is this the greenest Glasto?") Greenpeace and XR joined forces for the first time for an Extinction Procession through the festival culminating in creation of the largest ever XR symbol in King’s Meadow. Thousands of rebels heard speeches from XR’s Dr Gail Bradbrook and Greenpeace’s Rosie Rodgers. Among the **‘Rebel for Life’** banners and murals of Greta Thunberg asking **'What would Greta do?’**, there was even an appearance from the David Attenborough.

Elsewhere, [1,147 doctors signed an open letter in support of nonviolent direct action]("https://rebellion.earth/2019/06/27/more-than-1000-doctors-endorse-nonviolent-direct-action-to-treat-the-cancer-of-climate-change/" "Open letter signing") as they foresee a public health caused by climate change. [Three XR members met with Michael Gove](https://rebellion.earth/2019/06/26/extinction-rebellion-uk-follow-up-meeting-with-michael-gove-and-nadhim-zahawi/ "XR members meet with Michael Gove") for a follow-up meeting to discuss the government’s proposed net zero target and Citizens’ Assemblies. [XR disrupted a Royal Opera House screening in Trafalgar Square]("https://www.theguardian.com/environment/gallery/2019/jun/12/extinction-rebellion-protest-bp-trafalgar-square-royal-opera-house-romeo-and-juliet-in-pictures" "XR disrupt") to put pressure on the institution’s continued sponsorship by BP.

[Actor Sir Mark Rylance quit the Royal Shakespeare Company](https://www.theguardian.com/stage/2019/jun/21/mark-rylance-resigns-from-royal-shakespeare-company-rsc-over-bp-sponsorship "Sir Mark quits Royal Shakespeare Company") over their own sponsorship by BP and was [later joined by Miriam Margolyes in his boycott](https://www.theguardian.com/culture/2019/jun/27/miriam-margolyes-joins-mark-rylance-rsc-boycott-over-bp "joined by Miriam Margolyes").

[XR took a summer holiday last week to the Cannes Lion Festival](https://www.youtube.com/watch?v=nGvEeaRWBNU "XR summer holiday") – an event for marketing and communications industry. They were pushing for the ad industry to tell the truth about climate change with [protests outside the venue](https://www.thedrum.com/news/2019/06/17/extinction-rebellion-stage-cannes-lion-climate-protests "Protests outside the venue") and at [Facebook’s private beach](https://www.thedrum.com/news/2019/06/20/extinction-rebellion-brings-protest-cannes-facebooks-private-beach "Facebook's private beach"). Unfortunately, this led to heavy-handed tactics by the French police. It was nothing however in comparison to the repressive actions of the Parisian forces against a peaceful protest last Friday. [French rebels came together to occupy the Sully bridge](https://www.commondreams.org/news/2019/06/28/hottest-day-history-france-world-told-do-not-look-away-police-tear-gas-climate "French rebels came together to occupy the Sully bridge") in the heart of the capital in a joyful and nonviolent act prompting the completely excessive and unwarranted use of tear gas and removal by force. [Check out this shocking video](https://www.bbc.com/news/av/world-europe-48807155/french-police-pepper-spray-paris-climate-protesters "BBC Video") on the BBC website. There was some sort of tragic irony that all this occurred during a [week-long European heat wave](https://www.theguardian.com/world/2019/jun/24/hell-is-coming-week-long-heatwave-begins-across-europe?fbclid=IwAR26cx7Qn4IcxV_1k8F-LUuej7OyqRwPPuLmrl66HRXQfgD9OzBGIM0UERM "European heat wave") linked to climate change. A rather more successful protest by Ende Gelande saw [hundreds of activists occupy the Garzweiler coal mine](https://www.france24.com/en/20190622-hundreds-climate-activists-occupy-giant-german-coal-mine "Coal mine occupation") in Western Germany.

Although the [depressing narrative on how industries are killing us](https://ecohustler.com/article/one-corporation-to-pollute-them-all/ "Industries are killing us") continued, there was some glimmer of hope from India where one municipality passed a law requiring at least two trees to be planted for new building permits. [The UK’s first hydrogen train prototype](https://spectrum.ieee.org/energywise/transportation/alternative-transportation/all-aboard-uk-first-hydrogen-train "Hydrogen train prototype") was demonstrated in Warwickshire and The Guardian [suggested we start working a four-day week](https://www.theguardian.com/commentisfree/2019/jun/21/help-the-planet-work-a-four-day-week?CMP=Share_iOSApp_Other "four day week") in order to cut emissions.

Locally, [Youth Strike 4 Climate Manchester protested in St Peters Square](https://www.manchestereveningnews.co.uk/news/greater-manchester-news/climate-change-protest-manchester-city-16465518 "YouthStrike4Climate protest") and marched towards Oxford Square. An otherwise successful protest was [marred by a heavy police presence](https://www.manchestereveningnews.co.uk/news/greater-manchester-news/greater-manchester-police-collecting-evidence-16481957 "police presence") and frightening surveillance tactics by police Evidence Gatherers. In our last newsletter, we told you about the three Manchester schoolchildren punished for attending the climate strikes. Well, [the petition gained 5,685 signatures and global press coverage](https://www.independent.co.uk/news/uk/home-news/student-climate-strike-prom-ban-albany-academy-lancashire-a8971016.html "petition success"). On a lighter note, the Deputy Prime Minister decided to take the very [environmentally-friendly option of a helicopter for a flying visit to the city](https://www.manchestereveningnews.co.uk/news/greater-manchester-news/deputy-prime-minister-squirms-admits-16501114 "helicopter politician").

[Manchester Climate Monthly](https://manchesterclimatemonthly.net/the-calendar/ "Manchester climate monthly") still needs your help to put pressure on the council to declare a climate emergency. The [Manchester Climate Emergency e-Petition](https://democracy.manchester.gov.uk/mgEPetitionDisplay.aspx "e-petition") has already reached 455 signatures, but you can still get signing. A motion calling for the declaration of a climate emergency put forward by councillors Eve Francis and Annette Wright will be debated on the morning of Wednesday 10th July by Manchester City Council. [Climate Emergency Manchester is urging people to lobby](https://climateemergencymanchester.net/2019/06/21/date-for-your-diary-weds-10th-july-manchester-city-council-debates-climate-emergency/ "lobby!") all councillors across the 32 wards.

## WAITING IN THE WINGS – UPCOMING EVENTS

### Celebration of 100 Weeks of Women in White At Preston New Road

![Women in White]({{ site.url }}{{ site.baseurl }}/assets/images/newsletters/20190702/women-in-white.jpg "Women in White")

Frack Free Greater Manchester is organising a show of solidarity with the Preston New Road protesters and a celebration of the 100th week at the site with a walk from the base to the fracking site. They’ve done so much to oppose Cuadrilla and FFGM are asking people to come out in support of their valiant efforts.

**[Celebration of 100 Weeks of Women in White At Preston New Road](https://www.facebook.com/events/451189468780132/ "100 weeks of women in white"), Wednesday 10th July, leaving Manchester at 9:30, Maple Farm, Preston New Road. Contact Alan Challenger @FFGM for more details.**

### XR Workshop for People Considering First Time Arrest

![XR Training]({{ site.url }}{{ site.baseurl }}/assets/images/newsletters/20190702/training.jpg "XR Training")

We are running two training events to better inform members about the risk of arrest. These sessions will cover encounters with the police, what happens when you're arrested, the court process, and the possible consequences of getting convicted. XR MCR holds the safety of our members of the utmost importance. We strive to be responsible in encouraging people to consider arrest and make an informed choice. If you’re thinking of getting arrested then this event is essential.

**[XR Workshop for People Considering First Time Arrest](https://www.eventbrite.com/e/xr-workshop-for-people-considering-first-time-arrest-tickets-63960992049?aff=Arrestees10July "Workshop tickets"), Wednesday 10th and Thursday 18th July, 6:00pm-9:00pm, Friends’ Meeting House, 6 Mount Street, Manchester. On the 10th it is in room F11 and on the 18th room G1.**

### Manchester Climate Change Conference 2019

![Manchester Climate Change Conference 2019]({{ site.url }}{{ site.baseurl }}/assets/images/newsletters/20190702/climate-change-conference.jpg "Manchester Climate Change Conference 2019")

Next Monday is the 2019 Manchester Climate Change Conference where you’ll have a chance to hear from organisations at the forefront of helping the city to reach its carbon zero goals. The conference will be one part talk on what the city has achieved do far, and second part discussion where you can add to the conversation. Get your free tickets [here](https://www.eventbrite.co.uk/e/manchester-climate-change-conference-2019-tickets-61092866408 "Free tickets").

## Upcoming Events

Check out our [events](/events "XRMCR Events") page for details of upcoming events.

## Would you like to write for this newsletter?

Send us an email at [hello@xrmcr.org](mailto:hello@xrmcr.org "Email xrmcr").

> This is not a slow movement of change. It’s a shift in the consciousness of each of us.
