---
layout: single
author: grahame
title: A Social Survey on Climate Change
author_profile: true
categories: [Climate Change]
tags: []
share: true
excerpt: "Politicians lack the courage to tell the truth and the democratic mandate to act on the climate science. 80% of the public care, but very few care enough. XR will change that. This blog looks at the evidence that 3.5% of the population is needed to bring the change we seek."
---

## A word from the author

> Politicians lack the courage to tell the truth and the democratic mandate to act on the climate science. 
> 80% of the public care, but very few care enough. XR will change that. This blog looks at the evidence that 3.5% 
> of the population is needed to bring the change we seek.
>
> -- Grahame

## Why I joined XR
Here is why I joined XR. We are destroying the natural world. There are pockets of good governance, but at a global 
level our biosphere is effectively ungoverned. The world as we know it depends on our biosphere. If the biosphere is 
ungoverned we are ungoverned. 

I'm new to protesting, so I started by taking a look at the research that underpins the XR principles and values. 
I did this for me because I need to know we can win our argument in time. It is not certain we will win, but if we 
remain complacent we will certainly lose. Government and the governed are complacent. Most countries are. Despite 
the fine words and the agreements, we are addicted to consumption. There are two sides to the solution. First, 
is what governments must do - greening our infrastructure and manufacturing to allow our current consumption to carry 
on. But number one is restoring our natural world. Lots of people support these measures. But it is way too late to 
wait for that to happen while our lifestyles stay the same. We must also reduce our consumption until the pips squeak. 
If you are interested, here is a summary of my analysis, with links.

## Data Analysis

> 3.5 million extremely worried people
> and about 12 million very worried people

In the UK 1 in 4 are extremely worried (6%) or very worried (19%) about climate change 
([British Social Attitudes to Climate Change 35, 2018](http://www.bsa.natcen.ac.uk/media/39251/bsa35_climate_change.pdf){:target="_blank"}). 
That is 3.5 million extremely worried people and about 12 million very worried people. They didn't have a category for
how worried I am. The numbers vary across the political spectrum and with age. What we see is that the younger and 
politically left are slightly more worried. But the numbers don't vary that much: there are Extremely Worrieds 
everywhere. Another way to look at it is that 4.5 million adults are members of environmental groups. I am sure 
there is a lot of double counting here, but there are also a lot of young people not counted in the study 
([Passionate Collaboration, 2013](http://www.greenfunders.org/wp-content/uploads/Passionate-Collaboration-Full-Report.pdf){:target="_blank"}).

> 60% 'feel the government is not doing enough in
> preparing for and adapting to climate impacts'

So it may be that about 1 in 10 adults in the UK is a member of a not-for-profit environmental group. As well, there are
many other not-for-profit groups with climate chaos related interests - justice groups, animal welfare groups - plenty 
of them. The social attitudes survey also reports that about 15% feel a personal responsibility to reduce climate change.
That's about 10 million people. Another survey 
([Client Earth, 2018](https://www.documents.clientearth.org/wp-content/uploads/library/2018-08-20-clientearths-climate-snapshot-coll-en.pdf){:target="_blank"})
finds that 60% 'feel the government is not doing enough in preparing for and adapting to climate impacts', 
while 80% want the UK to honour its Paris agreement commitments. This is an immense force for change even if we
exclude the many people who can't face the horror or simply have no space to add this momentous issue into their lives. 

## XR Principles and Values

> __no non-violent direct action campaign that had__
> __3.5% of the population actively engaged has failed__
>
> Erica Chenoweth
> <br>
> [The Success of Nonviolent Civil Resistance.](https://www.nonviolent-conflict.org/resource/success-nonviolent-civil-resistance/){:target="_blank"}

XR (Principles and Values flyer) says we need 3.5% of the population to achieve systems change. This number comes from 
the amazing and very influential work of Erica Chenoweth, who started out wanting to join the army and ended up writing
a PhD on the power of non-violent protest. In the UK that 3.5% is about 2.5 million activists prepared to engage in 
non-violent direct action or the supporting logistics, resulting in a significant number being arrested. She says - 
'no non-violent direct action campaign that had 3.5% of the population actively engaged has failed'. Wow.


So it looks as if the numbers are clearly there. We should be able to reach this threshold for change.
[Erica says about XR](http://www.bbc.com/future/story/20190513-it-only-takes-35-of-people-to-change-the-world){:target="_blank"}, 
"They are up against a lot of inertia. But I think that they have an incredibly thoughtful and
strategic core. And they seem to have all the right instincts about how to develop and teach through nonviolent 
resistance campaigns." But we need to be careful here. We are not overthrowing the kinds of regimes that Erica's
analyses focus on. Her studies are about oppressed peoples seeking the freedoms that we take for granted.
 
She said in [The Guardian](https://www.theguardian.com/commentisfree/2017/feb/01/worried-american-democracy-study-activist-techniques){:_target="blank"}
that historical studies suggest that it takes 3.5% of a population engaged in sustained nonviolent resistance to topple 
brutal dictatorships. If that can be true in Chile under Gen Pinochet and Serbia under Milosevic, 
a few million Americans could prevent their elected government from adopting inhumane, unfair, destructive or oppressive
policies – should such drastic measures ever be needed. 

> __Let the party continue or opt for repression [and this will]__ 
> __bring down the regime__
>
> Roger Hallam

So perhaps we need far less than 3.5% as we are not living in a brutal dictatorship. Roger Hallam says we need more than
several thousand and ideally 50,000 in the UK (In the XR handbook This Is Not A Drill). We go to the capital, we are 
non-violent but we break the law. The state is disrupted and this brings economic cost. We get arrested, the world 
watches on, and governments have a dilemma: "let the party continue or opt for repression", as Roger says, 
and this will "bring down the regime. It's very quick: around 1 or 2 weeks on average. Bang: suddenly it's over". 

But perhaps it's not less than 3.5%, perhaps it's more. The poll tax took 10 million people. And that was a single 
issue with a simple solution. The obvious point about brutal dictatorships is that they are unpopular. Dictators can't 
hide inside a democratic process. Their brutality and absolute authority makes them vulnerable. They are unstable: you 
can topple them. Our regime emerges from a democratic process no matter how dreadful you think it is. Because of this 
I take the XR use of the word 'regime' to mean the 'way we do things now' rather than 'an authoritarian government'. 

When Roger says - "let the party continue or opt for repression", I think our government (regime if you like) could say
the same thing with the same dilemma the other way round. "et the party continue" is this feast of consumption, waste 
and destruction we are in now with the complicity of most people, "or opt for repression" - what Hazel Healy calls 
"eco-fascism" (This Is Not A Drill). For the government it's between the devil and the deep blue sea. 
But, it needn't be.

> __What collapses a regime is when insiders turn against it__
>
> Mark Almond
> <br/>
> [How revolution happens: Patterns from Iran to Egypt](https://www.bbc.co.uk/news/world-middle-east-12431231){:target="_blank"}

Mark Almond says, "What collapses a regime is when insiders turn against it" . So the first role of our protest is to 
persuade enough of the insiders of government and parliament that there is no other option. But also and just as
important, they must see that there is a way through. We don't have to prove the science: this is agreed by all but 
'flat-earthers'. No one suggested to Greta that she didn't need to worry. I am sure many if not most MPs agree with us 
on the need for precipitous action. But they are trapped. Trapped in the short term, prioritising the urgent needs of 
constituents, managing the bureaucracy and the internal battles of their parties, fire fighting their inboxes, up to 
their necks in stuff to get done, worrying about getting re-elected. They can't see the way through. We can help them 
here. We keep this climate emergency centre stage. And keep it there until insiders turn against this shameful lack of
action. I really think we are pushing on an open door and Mark is absolutely right. But to act requires a plan, because
the government simply doesn't know how to deliver the transition to zero carbon.

This all means that it is easier to bring the government to a negotiation but harder to change it. Democratic systems 
are messy, and all the parties will have an eye on the voters. We will make rapid progress with fewer numbers as the 
government will be willing to talk (it's not a dictatorship) - but we will have to keep going for longer and work 
harder for wide-scale public support. Just as with the overthrow of brutal dictatorships, the people have to support 
the rebels. But, unlike with a dictatorship, we can't take this support for granted. Social attitude surveys show that
not enough people are yet ready to support the changes we need. So while 80% want to see the UK meet its (inadequate) 
Paris agreement targets, only 15% feel a personal responsibility to change. This goes to the heart of one of the XR 
demands - that the truth is spoken. We make this demand of our government and it is key. 

I want to make one policy recommendation for government. The budget to meet the climate change reduction targets has to 
be prioritised and ring fenced. Apart from this it is business as usual between the political parties. This means the 
policies and tax and spend decisions of the other departments must be partly subordinate to the overarching requirements
of the climate emergency. 
