---
layout: single
author: default
author_profile: true
title: "XRMCR Newsletter 13:  Mushrooming Into Insurrection"
categories: [newsletter]
tags: []
share: true
---

**It’s gearing up to be an amazing week of events at XRMCR and today’s newsletter will make sure you don’t miss a thing. Don’t forget that Project Mushroom starts next week and Leeds is hosting the summer uprising in the North from Monday 15th to 19th July.**

## Latest News

In the wider XR world, [Pride in London declared a Climate Emergency](https://rebellion.earth/2019/07/06/extinction-rebellion-welcomes-pride-in-londons-declaration-of-a-climate-emergency-pride-is-a-protest/ "Climate Emergency") after we called for the organisation to meet our three demands. During last week’s annual Pride Festival in London, [XR LGBTIQ](https://www.facebook.com/groups/1838176052976311/ "XR LGBTIQ") also commended the organisation for committing to go carbon neutral by 2020. If you missed all the Glastonbury exploits last weekend then catch up with [XR’s latest podcast](https://rebellion.earth/2019/07/05/glastonbury-special-extinction-rebellion-podcast/ "XR Podcast").

In our neck of the woods, the [University of Manchester declared a climate emergency](https://www.staffnet.manchester.ac.uk/news/display/?id=22224 "Climate Emergency Manchester University") and we held our first People’s Assembly at the weekly Monday meeting. If you’ve never been a meeting and you want a glimpse of what we do, check out this [video by Jessica Stone](https://www.youtube.com/watch?v=mTW-lhINqvs "Video by Jessica Stone").

## Can you host for the Northern Rebellion?

We’re setting up our own Rebel Airbnb! If you can offer activists from outside Manchester a bed, sofa or even a space to pitch a tent in your garden, email [northernxr@gmail.com](mailto:northernxr@gmail.com "Email Us"). We’re also looking for volunteers to steward. Please email the same address with the subject ‘steward’. Anyone with free access to a PA system, large sound system, battery or solar generator, staging, curtainsider truck, megaphones, high –viz, festival tent/gazebo, barriers/railings and catering equipment is asked to get in touch with the subject line ‘kit’.

## We’re collecting funds for the Northern Rebellion next month

Please support our [gofundme page](https://www.gofundme.com/extinction-rebellion-manchester-actions-and-events "Gofund me").

**Coming up we’ve got a round-up of past and future events, the Cap and Share debate continues, there’s an uprising and free pizza...**

## A HIVE OF RECENT ACTIVITY

### XRMCR’s People’s Assembly

> People’s Assemblies are a way for a group of people to discuss issues or make decisions collectively, where all voices are heard and valued equally and no one person or group are able to dominate the process.

Last week you got the chance to make your voices heard on the direction of XRMCR. Key themes emerged on recruitment, the benefits of social/face-to–face meet ups, finding out how to get involved, as well as what’s going on in local and working groups. In the future we’ll make sure the meetings are publicised in advance and include more updates in addition to training in new skills.

Remember **we are all crew** and you’re welcome to get involved by contacting the relevant working group on our [contact page](/contact "Contact Us").

## BEE IN YOUR BONNET

**Last week Paul Harnett replied to Grahame and Elliot’s thoughts on his Cap and Dividend proposal. Here’s their response:**

_Thanks for your response. You have clarified the cap and dividend proposal, and we understand it better now. There are two sides to the proposal. The first is an absolute and global cap on production. The second is that some part of the revenue from the remaining production under the cap is set aside for a universal income. This is the dividend. You use an analogy of a tap and a sprinkler. The cap is turning down the tap. This you state is a simpler solution than blocking up all the holes on the sprinkler which represents the regulation and decarbonisation of all the industries that use fossil fuels. You tell us that turning the tap down is simply about controlling the behaviour of a small number of oil companies._

_We still have concerns. Firstly, it is not about oil companies directly - it is about nation states, and it is nation states that will negotiate any agreement for a global cap. As we said before, almost all fossil fuel extraction is by or for nations. The concerns you discuss in regard to oil companies are simply moved to the behaviours of governments. Governments support the extraction of fossil fuels because it makes them a lot of money, floats our economies and buys influence. Of course, this use of fossil resources is at the heart of our climate emergency, but many states still think it is in their interest and will fight to maintain it. The key point is that nation states won’t all agree to a universal cap. A universal cap, though very simple in principle, is not in practice. To suggest that this is the simple solution misses the point that the complexity of the ‘sprinkler' is just moved somewhere else - to a massively protracted global negotiation that the oil producing nations will stretch out as long as possible. And in line with your views, the oil companies will probably be happy to see this negotiation drawn out. These negotiations could take a lifetime - and we haven’t got that long. Even if an agreement was reached quickly through some lashed up compromise that set the cap way to high, it would be broken by producers defecting from the agreement and finding willing buyers._

_The best we can hope for is an alignment of likeminded states with high ambitions prepared to work together through beneficial agreements. And, that these relationships are seen to be successful and spread quickly to other nations._

_The second point is that even if we did go for this cap, governments will still manage change at the sprinkler. Turning the fossil fuel tap off without making lots of adjustments at the industry end would be chaos. We aren’t just making the downstream smaller - we are changing it. And the truth is that this change doesn’t happen without a lot of intervention. Perhaps it won’t be governments as we know them now. Perhaps it will be more like the world that Hazel Healy describes in the XR handbook ‘The Is Not A Drill’, but it is still a necessarily structured intervention. Just look at the power sector in the UK and the effort required to decarbonise it. That is what governments do — they regulate and direct._

_Finally, the universal income need not come from a global pot supplied by fossil fuels. It could come from any government revenues that a state chooses. Your scheme requires two very difficult things to happen together. This makes it very unlikely to succeed. It would be better to disconnect these two activities. It will be easier to get governments to buy into a universal income if it is not dependent on the success of achieving a global cap on fossil fuels._

**If you’ve got a bee in your bonnet about anything XR and environment related, send your thoughts to [hello@xrmcr.org](mailto:hello@xrmcr.org "Email Us") with the subject, ‘newsletter’.**

## WAITING IN THE WINGS – UPCOMING EVENTS

### Project Mushroom – XR Summer Uprising

![Project Mushroom]({{ site.url }}{{ site.baseurl }}/assets/images/newsletters/20190709/uprising.jpg "Project Mushroom – XR Summer Uprising")

XR’s summer uprising demands the government act now to halt biodiversity loss and reduce greenhouse gas emissions to net zero by 2025. Get yourself to Leeds from 15th to 19th June for a week of rebellion, music, food, speakers and much more. The action starts at 10am on the Monday in Leeds City Square and rebels are asked to wear yellow (not hi-vis). We also have NVDA Training this Sunday in preparation for the uprising.

**[Project Mushroom – XR Summer Uprising](https://www.facebook.com/events/2346725045567238/ "Uprising Event"), 10:00am, Leeds City Square**

**[NVDA Training before Operation Mushroom](https://www.facebook.com/events/643821866086000/ "NVDA Training"), 12:00pm-3:00pm, Room 2.5 Manchester Student Union**

### Manchester City Council Debates the Climate Emergency Motion

![Climate Emergency]({{ site.url }}{{ site.baseurl }}/assets/images/newsletters/20190709/climate-emergency.png "Climate Emergency")

A motion calling for the declaration of a climate emergency will be voted on this Wednesday 10th July by Manchester City Council. [Join us to support the councillors](https://www.facebook.com/events/620989675058943/ "Join us event") making this historic decision. Gather outside at 9:30 and then attend the meeting in the public gallery.

[Climate Emergency Manchester is urging people to lobby all councillors across the 32 wards](https://climateemergencymanchester.net/2019/06/21/date-for-your-diary-weds-10th-july-manchester-city-council-debates-climate-emergency/ "Climate Emergency Manchester urges your participation"). You can also sign the [Manchester Climate Emergency e-Petition](https://democracy.manchester.gov.uk/mgEPetitionDisplay.aspx?Id=20&link_id=7&can_id=4c1e45e42bae1c5ba9ea85cf4c2bff68&source=email-big-weekend-of-xr-events-please-come-along-plus-news-round-up&email_referrer=email_545196&email_subject=big-weekend-of-xr-event "Manchester Cliamte E-Petition").

**[Manchester City Council Debates the Climate Emergency Motion](https://www.facebook.com/events/620989675058943/ "Manchester Climate City Council Debate"), 10th July, 9:30am, Manchester Town Hall Extension.**

## Climate Emergency! Where’s the Urgency?

![Climate Emergency! Where’s the Urgency?]({{ site.url }}{{ site.baseurl }}/assets/images/newsletters/20190709/manchester-declares.jpg "Climate Emergency! Where’s the Urgency?")

With estimates of 15-50% of total emissions deriving from animal agriculture the only way to achieve XR's demand is to transition by 2025 to a Plant Based Society. In October, Animal Rebellion will take to the streets alongside XR to demand the same: that the government tells the truth about animal agriculture emissions, legislates for a plant based society by 2025, and sets up a citizens’ assembly to deliberate on how to achieve this. [Come and see a presentation](https://www.eventbrite.co.uk/e/climate-emergency-wheres-the-urgency-free-pizza-tickets-64799935351?aff=esfb&utm-source=fb&fbclid=IwAR3GJZ5Cq3ZtWKwwZ7diDrBZRsyVxKGgAfh6fau9Hyu8uGBVvytF_Skuwn0&utm-campaign=social&utm-content=attendeeshare&utm-medium=discover "Mark Lancaster Presentation") by Mark Lancaster from Animal Think Tank who is well versed in national XR strategy. Take part in the discussion - plus free vegan pizza from Zad’s. If you haven’t tried Zads, then you’re in for a treat!

**Climate Emergency! Where’s the Urgency? July 10th, 7:30pm-9:30pm, Yard Theatre, M15 5RF.**

## Fast Fashion Die-In

![Fast Fashion Die-In]({{ site.url }}{{ site.baseurl }}/assets/images/newsletters/20190709/fast-fashion.jpg "Fast Fashion Die-In")

XR Stockport have been busy lobbying council and leafleting but this Saturday they’re holding a coffin procession and die-in at Merseyway Shopping Centre to highlight the impact of fast fashion. Rebels will ‘die’ for 11 minutes to represent the years we have left to avoid climate change. Please wear dark clothes and meet at Hillgate Cakery at 11:30am.

**[Fast Fashion Die In](https://www.facebook.com/events/434164280512709/ "Die In Event"), 13th July, 11:30am-12:15pm, Hillgate Cakery, then Merseyway Shopping Centre.**

## XR Northern Rebellion Big Arts Meeting

If you signed up for a job at the jobs fair or you want to show off your artistic talents, then [come and help us create art for the Northern Rebellion](https://www.facebook.com/events/2100189340283683/ "Come help us create art!"). We will be splitting into mini arts groups including musicians, resources, banners and signs, theatre, artist outreach, sculpture, design etc. Please bring any ideas for the themes 'Rebel Festival': 'Oasis' (nature, not band!) and 'Eco workshops' and some coloured pens if you have them. This will be a great opportunity to meet up and make connections.

**[XR Northern Rebellion Big Arts Meeting](https://www.facebook.com/events/2100189340283683/ "Arts Meeting Event"), 10th July, 6:00pm-8:00pm, venue to be announced.**

## Social and Regen XR Walking Day Out

Need a break from all the doom and gloom? Avoid burn-out and meet with other activists for a walk in the peaceful tranquillity of the Peak District.

**[Social and Regen XR Walking Day Out](https://www.facebook.com/events/426574651272705/ "Social and Regen XR Walking Day Out"), 14th July, Hathersage, Peak District. Meet at Piccadilly Station for the 9:45 train. Train back at 4:30pm.**

## Upcoming Events

Check out our [events](/events "XRMCR Events") page for details of upcoming events.

## Would you like to write for this newsletter?

Send us an email at [hello@xrmcr.org](mailto:hello@xrmcr.org "Email xrmcr").

> There’s no Pride in Ecocide.
