---
layout: single
author: stockysatlarge
title: "Update: round up of news from XR MCR (3rd May)"
author_profile: true
categories: [news]
tags: []
share: true
---

> How you can get involved in XR MCR, news about our next meeting and other events, a report on our last big gathering, and why the #climateemergency declaration in Parliament is just the start.

### XR MCR - WHAT'S HAPPENING?

International Rebellion Week was a whopping success that has caused a massive shift in the urgency and legitimacy of conversation in the UK about the global climate crisis.

It has seen a huge surge in interest in XR across the UK including in Manchester and our key goal here in the city region is to make sure we can welcome all new supporters and grow the movement by making it as easy as possible for people to join, especially those who want to take an active role

As a group we're also now in a period of reflection to ensure we can learn for the future, as well as regenerate ourselves.

### NEXT MEETING

Monday 6th May at Breadshed, 126 Grosvenor St, Manchester M1 7HL.

The focus will be on matching up people who have things they’d like to do - from art, music and festivals to media, outreach and direct actions - with jobs that need doing - of which there are many :o)!

We will also share info on how to form a local group or a smaller support group (known within XR as an affinity group), and facilitate people to make local connections. And have some fun too!

Meetings will carry on in this vein for all of May - so if you can’t make this Monday (as we know many people will have plans for the Bank Holiday) then please just come along another time.

We will post updates here after each Monday meeting - and send via our newsletter.

### CLIMATE EMERGENCY

Wednesday’s Climate Emergency Declaration in Parliament - where many MPs thanked XR and School Strikers for raising this issue - is a fantastic step forward.

But will unfortunately remain hollow words if not swiftly followed by a massive mobilisation programme across all government departments, from transport to agriculture to energy to housing.

This does not look forthcoming - given the fact many contrary policies are continuing regardless - such as the expansion of Heathrow and most other airports and leaving it a whopping 31 years until 2050 for the UK to be zero-carbon - sadly it seems that not everyone is getting the meaning of the word ‘emergency’ yet….

So it’s vital we continue to push for action, especially here locally.

### MONDAY 29th APRIL - MEETING REPORT

Which is why it’s lucky we had a MASSIVE turnout when we gathered in a new, bigger, more accessible venue with gazebo, a tent, placards, banners and pink origami boats to celebrate and reflect on the rebellion week in London.

We were amazed by a turnout of more than 200 people, compared to a typical 30-40.

Four activists spoke movingly about their experiences over the last fortnight. Natalie, who went down to London as a legal observer, told of how she stepped in to help with the kitchen tent at Parliament Square, and spent 12 hours "feeding the 5,000" under challenging conditions. "Once you stand behind a table, everyone assumes you're in charge."

Ryan talked about the importance of XR's wellbeing team, and his experience of arrest. Kyle explained his learning from other regional groups, such as the power of close-knit "affinity groups". And Bethan spoke of her feelings as she pulled together our social media while watching us from afar.

Later we held a People's Assembly. Splitting into small groups by area of Manchester, we discussed two questions:

- How do we capitalise on the the success of London?
- How will we integrate thousands of new members?

The assembly threw up loads of ideas, some of which we will implement straight away, and others that groups will need to take away to plan further - and at least four or five new groups seemed to form on the night, with people making many new connections.

We also shared more about how we support each other through wellbeing and regenerative culture, which is a focus of the reflective phase we are now in, while also welcoming many new members.

### STAYING IN TOUCH

Here are the main ways ways to stay in touch with XR MCR.

- Our website [xrmcr.org](https://xrmcr.org "xrmcr.org") (under further development) which is where we will post articles and updates and event listings
- Our email newsletter which we usually send every 10 days or so
- Our [Facebook page](https://www.facebook.com/xrmcr/ "xrmcr Facebook") - where we share events, updates, articles and useful links
- A Twitter account @xr_mcr
- An Instagram account @xrmanchester

We are reviewing these external channels, and other internal methods we use - and really need volunteers to help with all of them!

So please get in touch if you have media skills you would like to share - or like to learn.

### DONATE

We are an entirely volunteer and unaffiliated group - our running costs are low but we do need help with things like flyers and other action materials.

If you would like to donate you can do so at our [GoFundMe page](https://www.gofundme.com/extinction-rebellion-manchester-actions-and-events "GoFundMe").

### UPCOMING EVENTS

Sun 5th May - XR MCR Go Gardening - For international permaculture day, Platt Fields Market Garden, 10-2pm

Mon 6th May - XR MCR regular meeting, 6pm, Breadshed

Fri 10th May - XR MCR - Heading for Extinction & What To Do About It talk, Manchester Metropolitan University Student Union, 6.30pm

Fri 10th May - XR MCR Fundraising Event, with local live music, Sandbar, 4pm till late

Mondays 13th May, 20th, 27th May - XR MCR meetings, 6pm, Breadshed

Sat 11th May - This Changes Everything - free Naomi Klein film screening, 2pm, Methodist Central Hall

Tue 14th May - End Climate Chaos - Climate Keys & XR MCR - spoken word, music and poetry - plus an audience conversation about Citizen's Assemblies . 8pm, St James & Emmanuel, Didsbury

Thur 23rd May - What next for Climate Action in Manchester? Open meeting including Manchester green groups, 7pm for 7.30, Friends Meeting House, 6 Mount St

Sun 25th May - Envirolution Festival, Platt Fields - XR MCR will have a stall. WE'RE LOOKING FOR MORE VOLUNTEERS TO HELP WITH THIS EVENT - PLEASE CONTACT US OR SEE THE FACEBOOK EVENT TO HELP OUT.

### THOUGHT FOR THE WEEK

> 'Change is constant. Be like water. In a cup it becomes a cup. In a teapot it becomes a teapot. Water can drip and it can crash'.
