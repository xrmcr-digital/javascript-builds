---
layout: single
author: ian_leather
author_profile: true
title: "A truly new world"
categories: [opinion]
tags: [debt, government, profit]
share: true
---

I wrote a document borne out of frustration when reading the political manifestos during the 2017 General Election and seeing virtually nothing that I could vote for. I just wanted to clear my head by putting on paper my ideas of what a ‘Great’ Britain could look like. Then I read a book ‘No Is Not Enough’ by Naomi Klein. Whilst this mostly tackles the issues of the rise of Donald Trump and the consequences of him coming to power, it also suggests that **we can no longer carry on being a consumer society not because of a debt crisis but the planet can no longer sustain it.**

Although she does not raise the issue of debt, her explanations on how all the power and wealth has found its way into the hands of an elite 1% and how, **after decades of conforming to this norm, people power is slowly but surely rising to fight against the injustices**. I then did further reading and realised there are other options but it cannot be achieved by conventional ways of protest, so just kept these ideas to myself.

**So here we are now with Extinction Rebellion in the UK** looking to pursue a peaceful revolution and a voice for people like me and the team at XRMCR have asked me to write a blog based on my findings. The main document I wrote is over 6500 words long and contains much more detail and evidence but for the purpose of this blog here are the headlines:

> It will be impossible to achieve the climate change targets by negotiating with Government and their officials.

### The Evidence

Our Government, in fact all Western Governments, are totally wedded to the economic principles of Perpetual Growth, High Productivity, Consumerism and inextricably tied into Global Monetary Policies (of which they have no actual control, hence the bailing out of the banks with taxpayer’s money in 2008). And the banks are at it again by the way.

A recent report/speech issued by Mark Carney of the Bank of England on 12th February 2019 called ‘The Global Outlook’ is loaded with debt issues but this one is the most startling:

> The global leveraged lending market grew by 21% in 2018, faster than the rate of growth in US subprime mortgages in the run up to the crisis. At \$2.3 trillion, the stock of outstanding leveraged loans is double that of subprime in 2007. Leverage of the most indebted issuers has been increasing rapidly.

Talk to any politician or financial expert and whilst you might find a sympathetic ear about climate change, they will only offer solutions within these set of principles and almost inevitably tell us that any changes will come ‘at a cost’. **Therefore, I cannot see how campaigning for change in the traditional way will work.**

### A new enlightenment and putting ‘money’ in its place.

**If I said to you there is a book that advocates an economic model that will be an extension of the ecosystem, not a violation of it, would you read it?**

Within its core it has the following characteristics embedded in its principles:

- To restore the mentality of the gift to our vocations and economic life.
- To reverse the money-induced homogenization and depersonalisation of society.
- To promote local economies and revive community.
- To be consistent with zero growth yet foster the continued development of our uniquely human gifts.
- To promote a new materialism that treats the world as sacred.
- It will be aligned with political egalitarianism and people power and will not induce more centralised control.
- It will restore lost realms of natural, social, cultural and spiritual capital.

This book is **Charles Eisenstein’s “Sacred Economics. Money, gift and society in the age of transition”.**

It gives the reader a fantastic historical perspective on money and how it has come from humble beginnings to now completely running our lives and in turn ruining our planet. He then with great clarity leads the reader into the viable alternatives including:

- Negative Interest Economics
- The Gift Economy
- Internalisation of social and environmental costs
- Localisation of money

**May I be as bold as to say that this book (and other works he references) is an absolute essential read for anyone who wants to get their head around how we can peacefully transition to a new economic structure.**

### An action plan leading to a truly new world

If something as radical as this were to have any traction and not be crushed through fear of the unknown, I think there firstly has to be big a publicity campaign (something that could go viral on social media perhaps?) on why the current economic system is a complete failure, dispel the myth that "Money Rules The World" and demonstrate what a world without debt would look like compared to the madness we have now.

It needs to be so compelling that we get enough people to be convinced that an economic revolution _has_ to happen and then what you want is for them to be saying, "We get it now so what are the alternatives?" (much in the way the David Attenborough climate change programme has done).

### Then (and only then) is it time for direct action.

This is my proposal:

### A non-violent but highly effective ‘financial revolution’

What if, through social media, a campaign started asking for everyone who has any type of debt takes a payment holiday for one month? So let’s say 10,000 people in month one of the project stop payment on their mortgage, car loan, credit card balance and/or personal loans.

Once the people who take this brave step show to the rest that they are still in their homes, driving their cars and do not have bailiffs taking away their TVs etc. then they take a second payment holiday along with another 150,000 taking their first payment holiday and so on.

If you are renting your home - join in, there is no chance landlords can evict you as with enough numbers the courts would be clogged with eviction orders so they (the landlords) might as well join in and stop paying the banks their mortgage payments.

At this point the Government will yield the full force of the media to scare us all witless with tales of impending doom, however the people stand firm and month after month more and more stop paying the banks and inevitably the banking system crashes.

"Oh no not again!" I hear you cry, we have just been through this but now the UK Government morally and fiscally cannot possibly bail them out again. Indeed they should, and probably will have to join in the project and tell their creditors (the mathematicians in the global banking system presumably) - **“Y’know what? We are not paying our debts either."**

So, potentially, in just a few months Government and personal debt of **£3.5 trillion** is cleared and all the banks that have fleeced us for decades are gone.

### So what happens then?

Whilst realising this will trigger a global banking crisis, I am going to stick to the UK;

Firstly the Bank of England secures all the money we had in these banks (FSCS £85,000 guarantee already in place but just extend this to an unlimited amount) and all house/business premises deeds, car titles etc. handed to the now true owners.

How about then legislating that all new banks have to be not for profit with an emphasis on local mutual/co-operative or business sector basis with The Bank of England handling national banking issues (based on ‘Sacred Economics’ type theory) and laws passed to prevent us ever going back to ridiculous mortgage loans (indeed any long term debt whatsoever).

Now I am not naïve enough to believe that this won’t culminate in a short term period of turmoil, but this is where any kind of popular movement towards this change must stand firm and offer calm and clarity as to what the ultimate goals are and guarantee we shall all be fed, housed and begin to look to local communities to come together to ensure no-one is abandoned.

### Can we then change our ways?

**It is simple; only spend what you can afford and without the oxygen of credit the corporate companies and their ad men will wither on the vine.**

This doesn’t mean a return to the dark ages either; there is so much existing product around. For example the 1000s of 2-4 year old ex-lease cars sitting in airfields that could be bought for next-to-nothing if my understanding of supply and demand theory is correct.

Those wedded to the financial markets might ask, **‘How do we know what the pound is worth if there are no markets?’**

The recent Bitcoin phenomenon shows that there is no reality to the value of any currency (this is just something a guy made up and put together an algorithm on his home computer). **Cash and currency should merely be a tool to trade goods and services in a way people can understand - not a play-thing to enrich the elite or form of control over the population by politicians.**

### The main benefits

- We all live debt free and become masters of our own destinies personally and communally.
- Whilst those that are cash rich will remain so, they will not be in charge of influential corporate lobbyists, therefore losing their political influence and they will either leave (the country) or stay and become philanthropic, valued members of society. Those so called rich elite who are cash poor and have huge (mostly undeserved or criminally gained) property/share/pension portfolios will of course be much less happy.
- Your home is now for living in (I believe that was always the intention) for you and your family and not a financial cash cow for some and a lifetime of debt for others.
- We will be a lot less busy chasing every last penny to keep afloat giving us more time to personally nurture our children (instead of relying on childminders/schools), pursue leisurely activities and look after our elderly community.
- Look at the coverage about the mental health and obesity issues in this generation of children. **We need to spend more time with them, giving them love and attention as opposed to giving them smart phones and X-boxes etc. as pacifiers because parents do not have the time.**

This will also be an opportunity to re-skill our workforce, encouraging even more self-employment and small local businesses supported by the new not-for-profit banks. Who knows - some trades and individuals will be happy to barter again in a new ‘Gift Economy'.

Volunteering in the UK is estimated to be worth anywhere from £45-80 billion a year, but in truth it is priceless. It has been gradually on the decrease as more and more of us struggle to find the time. This project would free up people to support community projects and sporting activities across all age groups.

### Further benefits of a debt free society

### Less waste

A simpler lifestyle including a return to simple foods eaten seasonally and, wherever possible, produced and eaten locally will have a dramatic effect on the current wasteful practices used by corporate companies in the pursuit of profits. We will clearly see health benefits too. With time and opportunity to become much more self-sufficient as individuals and as a country (because importing more than we export results in debt) we can encourage and support our farmers to produce what we need with any surplus given to those not so fortunate than us.

**In Daniel Quinn’s “My Ishmael” the most striking commentary is his theory that the downfall of society was when those in power ‘locked up the food’.**

### Criminal gangs

This culture is based on poorer people becoming obsessed with money and power in the same way as the ultra-rich elite. The poor and vulenrable are exploited. Whilst it is doubtful that this can be completely eradicated in a much more equitable society with people living simpler, healthier lives, surely this would take some of the heat out of this particular market?

### Fossil fuels

With the demand for fossil fuels massively reduced, and with policies in place to produce more renewable energy and make houses and buildings energy efficient, the **price of fossil fuels will crash**. Bad for the ultra-rich and Governments propped up by their wealth and influence but absolutely essential to avoid climate catastrophe.

**No demand = no profits and no _more extraction_**.
