---
layout: single
author: clare
title: Monday, 17th June '19 - Rebel Job Fair
author_profile: true
categories: [event, news]
tags: []
share: true
---

From 30th August - 4th September, the North will be rebelling here in Manchester. We’re going to need all hands on deck to make the government and other change-makers listen.

Monday 17th is a match-making session to link volunteers up with the roles that will be needed for the Northern Rebellion - whether you’re brand new to XR, want to get more involved or just want to continue working together.

All the working groups will be present with information on various jobs you can get involved with, as well as the skills and time commitment required.

From ordering eco portaloos to making art installations, any skills, help, and energy that can be offered will be needed over the coming months.

We’ll be at [The Bread Shed](https://www.bread-shed.co.uk/manchester "The Bread Shed") to chat from 6 with the session starting at 6:30. We expect the session to run until about 8:30.

The Bread Shed is wheelchair accessible and the introduction to and summing up of the session will be mic’d through a PA system.

Facebook event: <https://www.facebook.com/events/2319291301473387/?active_tab=about>
