---
layout: single
author: clare
title: Interviews from the YouthStrike4Climate – Friday, March 15th '19
author_profile: true
categories: [interviews]
tags: [YouthStrike4Climate]
share: true
excerpt: We talk in depth to five young people who are passionate about solving the planet's climate crisis as part of the growing movement YouthStrike4Climate.
---

### Meghan

I’m 16. [Climate change] has always been something I’ve been passionate about.

I think it’s so important for my generation, especially because we’re the ones who are going to have to deal with it. It’s something personal to me. It’s going to be our future.

I’m trying to do stuff in school. There is a teacher that I’m working with and she’s going to start doing an environmental club after school and stuff like that. But there’s not really much being done with our generation and I think that that’s something we need to focus more on.

Teach children more about it. Put it in the curriculum. Have special lessons on what we can do and encourage more people to act upon it. Protests like these are so good.

### What does your poster say?

> Why should we go to school if you won’t listen to the educated?

### Anna

![Save our Earth]({{ site.url }}{{ site.baseurl }}/assets/images/save-our-earth.jpg "Save our Earth")

My seven year old wanted to come. I’m here to support her.

She’s heard about it from me and a family friend. This was around the time of the last one in February, and she then said that she was really interested in doing it and I’ve looked out for another one since then. We’ve done a lot of reading up about the whole movement. So she’s really keen. She’s the only kid in her school who’s come today.

It’s an unauthorised absence. I’ve spoken to an older kid, maybe about fourteen, who’s got an authorised absence, so I’m quite optimistic about the fact that there are some schools with head teachers who are authorising it…. She [my seven year old] wants to carry on so hopefully there might be a few other families, maybe get the school interested. We’re going to take some photos back to the school. Hopefully they will encourage rather than discourage it.

It’s clearly spreading. I heard something about a Nobel Peace Prize this morning for Greta Thunberg. That’s incredible and only doing positive things.

I think it’s a positive educational activity for all children. How much more educational could it be really? I don’t care about the unauthorised absence.

I’m hopeful that it will [have an impact]. I don’t know whether it will. I’m just thinking back to the number of MPs that went to that recent debate. Just unbelievable. But this [the strike] is going to continue and we can only hope that people take notice. What I find really exciting is that there are so many committed young people here today. I’m astonished. That can only be a good thing in terms of the future – in terms of their future not mine.

There is that idea of your family, your legacy. It is significant and it ties up with the way that we treat the planet at the moment. I think having children just makes you that much keener, certainly for me.

<figure class="half">
    <a href="{{ site.url }}{{ site.baseurl }}/assets/images/interview-1.jpg"><img src="{{ site.url }}{{ site.baseurl }}/assets/images/interview-1.jpg" alt="Man reading into a microphone"></a>
    <a href="{{ site.url }}{{ site.baseurl }}/assets/images/interview-2.jpg"><img src="{{ site.url }}{{ site.baseurl }}/assets/images/interview-2.jpg" alt="Man reading into a microphone, gazing into a crowd"></a>
</figure>

### Leah

We’ve been brought up knowing about climate change. We’ve been brought up being taught about climate change in schools. I think that’s probably a really big factor in why everyone’s just so empowered to make a difference. We’re not denying climate change anymore. It’s in the curriculum now.

In uni I’ve been studying environmental philosophy and we’ve been really talking about how we can tax global goods: how can we tax environmental impacts; the oxygen we breath, how do we actually implement that into figures? For me, I just feel like whatever the upkeep costs are they should be added, not in terms of the taxpayer, but at the expense of businesses actually conducting the harmful activities. There should be actual laws – any change, any destruction… there needs to be a comprehensive plan on how they are going to reverse it and money put in to help regenerate the rest of the country that’s in the same situation.

It’s hard because there are so many things that could change. There’s so many factors, from the way that we eat to the way that we recycle, everything we do there is something that could be done, so it’s not a matter of one act. It’s hundreds and hundreds of acts that will come together to make a difference.

I feel as though there will be change, but by the time governments catch up it will be too late. I feel as though a lot of devastation will occur and a lot of people will lose their lives over selfish business transactions. It’s really sad to think we will keep on protesting, but it really will take a long time for this to be put into legislation. The Paris Agreement, nothing’s come out of that and it was such a big deal. I just hope that things like that won’t happen again but I really worry that they will.

Every single day when something that destroys the environment occurs, someone out there is being discriminated against. And that someone is you, and it’s your best friend, and it’s your mum. And so what excuse do you have not to stand up to that?

And there’s discrimination against other species… a lot of the beautiful diversity that we have is going missing. And every single action we make there is at least a species or a person that it being really, really affected.

![Running out of time]({{ site.url }}{{ site.baseurl }}/assets/images/running-out-of-time.jpg "Running out of time")

My dissertation was about why we should do civil disobedience and in what circumstances is it justified, what actions count as civil disobedience and what’s the difference between civil disobedience and revolution. It’s hard because a lot of theory is based around whether civil disobedience diverges for different democracies, depending on how justified of an institution they are. So for more western countries they say we’re just societies. But I feel like, in this day and age, climate change shows that there’s really no such thing as an actual fair democracy. In light of that we actually have to be fair to ourselves and really, really put forward things like this – put forward what we believe in.

We’re actually causing harm by doing nothing, rather than just being neutral. It’s like we do bad or we do good. There’s no neutral moral ground. Inaction is submission and then you’re saying that all the government’s actions are okay.

It’s just hard because we have such a comfort blanket in the UK. But we know that people in Indonesia, South-East Asia and in Central America are really going to suffer from our faults. It’s so much worse to be in submission. These are whole communities — countries — that will be put down for our mistakes.

### Phillipe

### Why did you come down today?

I think it’s vital, isn’t it? Youth striking for climate change. Why not? I’m an environmental scientist, so it is definitely up my street. But I think it should be up everyone’s street. This is a chance to start knowing about it.

I monitor carbon fluxes, CO2 emissions, so what’s causing climate change basically. I work on water sources here in the UK and as you know temperatures are rising, so peat lands, like in the Peak District, are releasing more carbon because there’s more rain, higher temperatures, so the emissions are just increasing. It’s a feedback loop.

We just need to make people care about it. Why wouldn’t they care about it? Food security, everything, it’s related to everything. It’s our planet. As Greta says, “Our house is burning”. We should all care. We should all care.

I don’t feel really positive, because the people in power just have really, really nasty interests. It’s difficult to change a system. It takes time and it takes influence. But if we’re going to survive as a species, we’re going to have to do it at some point. All these young people will rule the world at some point and I’m sure they’ll look back on this and say maybe that was the starting point. I hope so at least. That’s why I’m here.

I don’t really feel positive but I hope it will change. If we don’t adapt, we die. This is the adaptation. It’s slow. It takes time. It’s going to happen. It has to happen. Even if it’s just a few of us.

I can’t really say how it is back back home [in Mexico] but I think it’s still in its early stages. I’ve not even seen any protests back home. It’s coming from this side of the world, and it’s good because developed countries are the ones that ate up the planet initially. Initially at least. Now it’s other countries like Thailand or India, whoever really. It’s not about blaming anyone, it’s about now we know this: let’s acknowledge it, and let’s work on the plan.

![Modernity has killed us]({{ site.url }}{{ site.baseurl }}/assets/images/modernity-has-killed-us.jpg "Modernity has killed us")

It’s all related, isn’t it? Climate change, poverty, food security. A few have a lot while the rest of us don’t.

Islands in the Pacific or islands anywhere will be the first ones to feel it. But even here in the UK, don’t you remember last summer when there was, here in Manchester, two months without rain? The reservoirs that I study have been way below your average levels all because of last summer. And it’s going to keep on like that. This year, it’s not rained. We’re going to start feeling it. The UK thinks it’s safe in terms of water security. But it won’t be. That’s going to change. It’s going to be a massive problem for us. It’s going to be a massive problem.

It’s in everyone’s mouths. Climates are getting hotter, but what are we really doing about it? Are we investing? Are we changing the system? The system is the problem. We can all buy our electric vehicles or whatever but it’s not really a solution. It’s just temporary.

Every bit counts, I’m not being completely pessimistic. But we just have to acknowledge that we have to make more drastic changes.

We also have this responsibility as people to elect representatives that actually walk the walk. If we don’t inform ourselves and know about the problems that we are facing and choose people that are really ready to tackle them we’re going to be in the same problem forever. We have a choice. Look at all these kids. They’re saying it, they’re saying what everyone is thinking, to their faces. They’re just sixteen.

### Brian Doherty

I teach politics at Keele University. I’ve been following Extinction Rebellion and I’ve done quite a lot of work on direct action protests in the UK over the years.

Three of us are doing an in-depth study of the trial of the Stansted 15. We were in court every day for their trial and we’re doing our best to make sense of that, but also the anti-fracking sentences. So what I’m really working on at the moment is the courts and criminal prosecutions of activists and the way that the conditions for activists may be changing because of a combination of courts and policing.

### Do you think it’s becoming more harsh?

Yes, frankly. I think you can’t point to it as an orchestrated, singular event because you’ve got a combination of things that have been going on a long time, like the revelations we’ve been having recently with undercover policing. But I think we saw in the charges brought to the Stansted 15 and also the sentencing for the Preston New Road Three — we’re seeing changes in the practices of the courts where they’re happy to use much tougher sentencing and we absolutely think, the three of us working on this project, as do of course the defendants themselves in these cases, that this is a deliberate attempt to deter people from taking part in protests that had been accepted as a normal part of democratic citizenship.

I think that it will deter some people but I think that makes it that bit more important that we’re monitoring it and challenging it at every step. So things like the appeal for the Stansted 15, the solidarity that was shown to them because of the undoubtedly terrorism-related charges that were brought…

I think that it’s really important that those of us that are concerned about that do engage as much as we can because that really makes a difference in creating the kind of climate that allows or prevents the worsening of conditions for protests. We know that only relatively small numbers take part in the more demanding forms of direct action but at the same time we’re here at an event where you can imagine that some of those here today will be involved in future kinds of more challenging protests. And that makes it that bit more important that we don’t allow authorities to close down direct action, because it’s such an important mechanism for challenging power.

I’m optimistic, because we’ve seen this and also I suppose in the way that people have responded to Extinction Rebellion. You see people doing things that they haven’t done — different kinds of people doing things. That’s obviously a really positive step.

What worries me slightly about both is it’s very difficult to think what the next step afterwards is. I’m sure everyone here is — and folks in Extinction Rebellion are — worrying about the same thing. Because we all know that things need to be done. But it’s very difficult to use things like Fridays for Future, and Climate Strikes, and even the civil disobedience of Extinction Rebellion as a long-term campaign. I think the challenge for all of us is to think about how these tactics fit into a strategy for the next three to five years. I don’t have the answers to that.

I think those strategies are going to emerge from us creating spaces that sustain discussion. So people are coming out on days like this but on a day like this you can’t do planning and discussion. What I have noticed is that they’ve got weekly planning meetings and things, and what we need to make sure is that people are supporting those more mundane, more boring, organising kind of processes, because that’s where the strategy gets talked about. So I think the key to success is not just the spectacular events but the ongoing stuff.

![Lessons]({{ site.url }}{{ site.baseurl }}/assets/images/lessons.jpg "lessons")

---

```
Images by Molly Wilkinson, from Fridays for the Future
Interviews by Clare Carlile, member of XR Manchester
```
