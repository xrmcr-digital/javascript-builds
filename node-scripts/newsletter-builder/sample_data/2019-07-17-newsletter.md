---
layout: single
author: default
author_profile: true
title: "XRMCR Newsletter 14: A Momentous Moment for Manchester and Media Too Meek to Mention"
categories: [newsletter]
tags: []
share: true
excerpt: "Manchester City Council unanimously declared a climate emergency last week in an historic moment witnessed by climate groups, activists and citizens."

---

## A Momentous Day

Wednesday was a momentous day by all accounts. Not one but two climate emergency declarations in Greater Manchester. 
We cannot rest on our laurels, however. With the summer uprising in full swing we need the government to translate words 
into actions or we won’t stop fighting for our existence.

Manchester City Council unanimously declared a climate emergency last week. In an historic moment witnessed by climate 
groups, activists and citizens, Hulme Councillor Annette Wright put forward the motion by saying:

> “we need to state our intention much more forcefully and clearly than we have done already. We need to ensure that
> the issue of climate change is embedded into everything that we do and is top of our list of priorities”

Next up, Chorlton Councillor Eve Holt reminded everyone what is at stake in an passionate speech:

> “let’s be under no illusion, Climate Breakdown, and the wider ecological crisis, presents the most significant threat
> to ‘Our Fair City’ and to the safety and security of those she shelters. As well as representing the most serious 
> threat to all humanity in the entire history of our species”

Much deserved praise and recognition was given to the Youth Strikers represented by Hannah Mitchell of the Manchester
Climate Change Youth Board and Ishaa Asim MYP. After further speeches, a surprising amendment and a discussion on the
importance of the green economy, the motion was passed!

### Watch XR Member Claire Stocks video of that very moment
<figure class="youtube_embed youtube_embed--16-by-9">
    <iframe src="https://www.youtube.com/embed/chiEqiSTOOA" 
            frameborder="0" 
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
            title="Manchester City Council declares a Climate Emergency!"
            allowfullscreen></iframe>   
</figure>

Keep reading on for her account of the day and frustration at lack of media coverage for what was a significant moment 
in Manchester’s climate history. Thanks to the activists, councillors and groups that made this moment possible.

- You can read a more comprehensive version of events [here](https://climateemergencymanchester.net/support-the-campaign/councillors-and-the-july-10th-climate-emergency-motion-by-name/july-10th-climate-emergency-debate-videos-and-transcripts/?fbclid=IwAR1jmh2eN0o5RBTBWr-TQbl5B5aZaTzG5eoHqS4XNdK98oLQBrd4D0B48i4)
- Don't forget you can still sign the [Manchester Climate Emergency e-Petition](https://democracy.manchester.gov.uk/mgEPetitionDisplay.aspx?Id=20&link_id=7&can_id=4c1e45e42bae1c5ba9ea85cf4c2bff68&source=email-big-weekend-of-xr-events-please-come-along-plus-news-round-up&email_referrer=email_545196&email_subject=big-weekend-of-xr-event)

---

## Summer Uprising

We’ll be catching with all the goings on during this week’s Summer Uprising in our next newsletter. If you want to be 
part of it then head over to Leeds and [join in the action](https://www.facebook.com/events/2346725045567238/). 
As our own rebellion gets ever closer, we're still on the lookout for volunteers and aid. Please support our
[gofundme](https://www.gofundme.com/extinction-rebellion-manchester-actions-and-events) page.
 
We also need space to make and keep banners/resources. If you have an artist space, preferably in Central or 
South Manchester, please get in touch at northernxr@gmail.com with the relevant subject. 

Can you host for the Northern Rebellion? We’re setting up our own Rebel Airbnb! If you can offer activists from 
outside Manchester a bed, sofa or even a space to pitch a tent in your garden, email [northernxr@xrmcr.org](mailto:northernxr@xrmcr.org "Email xrmcr").

We’re also looking for volunteers to steward, nurses, medics and first-aiders. Anyone with free access to courier 
bikes, boats, or if you’d like to be a legal observer/arrestee support, let us know.

Don’t go anywhere because we’ve got a great newsletter coming up...

---

# Contents

- [A hive of recent activity](#a-hive-of-recent-activity)
- [Bee in your bonnet](#bee-in-your-bonnet)
- [Waiting in the wings - Upcoming Events](#waiting-in-the-wings---upcoming-events)

----
## A hive of recent activity

### Manchester Council Declares Climate Emergency

Here's XR member Claire's account of the highs - and subsequent frustrating lows - of this momentous day.

_09.45 Wednesday. Town Hall Portico, Manchester City Centre._

![Campaigners at town meeting, one holding a sign saying Climate Emergency Please Now Act]({{ site.url }}{{ site.baseurl }}/assets/images/newsletters/20190717/climate-emergency-please-now-act.jpg)

About 50 or so campaigners gather at the front entrance of the town hall extension ready to attend the Climate Emergency
vote being proposed by councillors Annette Wright and Eve Holt.

Chorlton councillor Holt had started working on the motion back in January – but was dropped swiftly when it seemed 
there was lack of support inside the Labour party (aka the council, given Labour’s 93-3 majority over Lib Dems).

In June we heard that it had been revived, and a campaign began, neatly corralled by Climate Emergency Manchester who've
gathered more than 2,000 signatures on a petition calling for the emergency, for people to contact their councillors to 
urge them to support it, and to push for key elements to be added to the motion.

(It does not pass me by that "ruler" Richard Leese is one of the new motion's signatories – I can’t help be curious at
what prompted his decision to back and whether it changed the tune of those who previously didn't, and wonder what it
was that persuaded him to do so…..prize for best guess!)

We troop up to floor 3 and the public gallery – noting there’s a security guard asking for ID on the door to floor 2’s 
press gallery and access to the chamber.

The agenda has been re-arranged so the motion is heard at the top of the meeting, which seems a suitable approach for
**An Emergency**.

Wright and Holt make brilliant, impassioned speeches met with standing ovations from the benches and galleries, Mandie 
Shilton Godwin makes a crucial interjection from the floor spelling out what councillors will need to do, and Lib Dem’s
young councillor Richard Kirkpatrick chips in with an amendment that is accepted without demure (unheard of!) to pull 
the carbon-zero target date back to 2030, and add a provision for the council to push the GM Pension Fund to divest 
from fossil fuel.

On the floor of the chamber are 40 or so school children, some littlies and some teens, standing with banners and 
placards which they hold facing the councillors in a ‘horseshoe of hope’.

![Placard stating Please be bolder]({{ site.url }}{{ site.baseurl }}/assets/images/newsletters/20190717/climate-emergency-please-be-bolder.jpg)

> Our Future in Your Hands
>
> Act Now!
>
> Join the Pollution Solution!
>
> Please Reduce CO2

Youth MP Ishaa Asim and another pupil are invited to the podium to address the council with powerful words.

The energy in the huge room – already hot when we arrived – is electric and the public in the ceiling gallery are 
sweating and breathless with excitement … (and an over-enthusiastic thermostat).

There are no objections. There is no point-scoring. There are only 96 democratically elected people doing their duty 
and doing it well.

The motion is carried and we applaud and we whistle and I have a lump in my throat.

In here, for this half an hour, the world IS reacting to the facts in a way those facts merit, we ARE going to do this!

I leave the chamber and pause to write a post on Facebook and Twitter.

I check the @mancitycouncil feed – nothing. Oh, perhaps they’re outside with councillors and pupils getting photos? 
Coz I assume the council have briefed them and teed up some coverage of this historic day?

I check the Manchester Evening News Twitter feed – nothing. Strange? I think – as I added the Newsdesk to my tweets. 
How easy is a simple retweet?

But by 3pm – four hours after ‘The Emergency’ was declared – still seemingly nothing on any media outlet – or the 
council’s own channels.

Our Manchester media, it seems, have not quite cottoned on to what ‘Emergency’ means.

So reality dawns – the council’s own press office has failed to cover it’s own Emergency – there is no press release, 
no imagery, no quotes, no clarification of what the motion means.

For instance – councillor’s Kirkpatrick’s amendment seemingly means that the city is now committed to 2030 not 2038 and 
seems a hugely significant point – but it is a not even recorded anywhere officially.

What’s the saying – if no-one sees it – has it happened?

This is what I feel like as I sit at home wondering whether I dreamt the whole episode.

I pick up my phone and call the MEN newsdesk.

I get a call back from a journalist who is apparently writing up the story by reviewing the live feed of the meeting.

I give her background, I give her some quotes. I send her photos. I send her a video of the crowd cheering the motion
being passed (which has had nearly 2,000 views simply from being shared from my own lowly Facebook page).

No story ever appears.

I’ve no doubt the council’s lack of action has not helped.

I send an email to the council’s Head of Communications.

I haven’t had a reply by the time of writing this, not that I expected one in that time given that I’d asked a number 
of questions.

But magically, two full days after **‘the Emergency’**, a piece appears on the council’s website (somewhat misleadingly 
badged with the date Wednesday 10th which may be when it was written but most certainly wasn’t when it was published, 
and I know because I’d checked multiple times in the previous 48 hours).

I’m so upset that the only places this event – an Emergency! – seem to have been recorded at the time are on personal 
social media and campaign accounts and websites (e.g. Extinction Rebellion, Friends of the Earth).

Councillor Holt manages to get hold of the speeches, edit them herself so she and Councillor Wright can publish on a 
personal YouTube page – the only mildly curated record of their epic calls to action.

Clearly, our climate and environmental crisis requires huge change – and such change can only come about with an 
engaged and active media. Yet the media outlets in Manchester seem to be virtually ignoring this agenda.

This communication and content failure is a matter of huge concern that any self respecting media professional in the 
city – wherever they work – cannot let continue.

> This is an abridged version of Claire’s article from her ['Bee the Change'](https://beethechangeblog.co.uk) blog.

---

## Bee in your bonnet

In our last instalment of the Cap and Share debate, Paul Harnett responds to Grahame and Elliot’s last comment.
Next week, we’ll address some of your comments on one of last week’s items.

> You are absolutely right - getting world governments to agree on a cap will be very difficult given present 
> circumstances but the point stands, it is the most effective way to hit zero carbon in the next few years.  
> Present circumstances see governments happy to subsidise the burning of carbon to the tune of about $5.2 trillion/year
> &ndash; so we are completely in the wrong situation to hit any targets.
>
> As for lobbying against, you are absolutely right - but the fossil fuel producers already have put aside funds to 
> prepare for significant taxes on Carbon. They are even lobbying for significant taxes on Carbon and it is now the
> situation that Green New Deal is seen as in opposition to taxes, especially in the USA but also in other countries.  
> There is no reason why you can't have a Green New Deal AND taxes/cap.  And your argument against a cap is exactly the 
> same as why taxes country by country won't work (and the carbon producers know this) - they will move operations, HQs,
> etc around the world to avoid taxes as far as possible - and the zero carbon targets will be lost again.
>
> So - if we got a few likeminded states to look for beneficial agreements (like Paris?) I suspect we would end up in 
> the same position, all piecemeal without decent enforcement.
>
> Also agree that capping carbon without preparing for transition would be disastrous, hence the view that a Green New 
> Deal should also be entered into. A massive retrofit of housing and investment into public transport, for example. 
> These would take governments to abandon the neoliberal orthodoxy of the present.  But also do not forget that prices 
> are still powerful signals.  If Carbon goes up in price then some - not all- transitions will occur naturally, e.g. 
> the vast number of journeys under 2 miles would suddenly drop as people take to bikes and walking.
>
> You are also right that UBI need not come from Carbon; there are many other potential sources, but the argument is 
> that it should come from the commons (however you define that) - and a basic commons is the air we breathe - so highly
> appropriate that it should come from pollution of the air (as well as other commons).
>
> The basic argument is that we have an international problem and we need an international solution - hence the 
> international rebellion!

If you’ve got a bee in your bonnet about anything XR and environment related, send your thoughts to 
[hello@xrmcr.org](mailto:hello@xrmcr.org?subject=Newsletter "Email xrmcr") with the subject "newsletter".

---

## Waiting in the wings - Upcoming Events

### Daily Dragon Slaying, Dancing and Witnessing
![Daily Dragon Slaying, Dancing and Witnessing]({{ site.url }}{{ site.baseurl }}/assets/images/newsletters/20190717/waiting-in-the-wings-daily-dragon-slaying.jpg)

The indefatigable Protectors at [Preston New Road](https://www.facebook.com/events/866749137019224/) need your support. 
Police have recently been a little more ‘hands-on’ as numbers protesting against Cuadrilla's fracking at the site have
dwindled. They’re seeking people to help monitor the site and spare a few hours, days or possibly longer if you can. 
They run regular events and have guest speakers each day of the week... plus they promise to always have a brew on 
the go.

--- 

### Extinction Rebellion: Connecting with Other Social Campaigns
![Extinction Rebellion: Connecting with Other Social Campaigns]({{ site.url }}{{ site.baseurl }}/assets/images/newsletters/20190717/waiting-in-the-wings-connecting-other-social-campaigns.jpg)

Extinction Rebellion Manchester is hosting an event to connect with other social campaigns in Greater Manchester and 
beyond. Various organisations will provide talks discussing what their organisation is doing. After that we will 
break into small groups to discuss how to support each other’s campaigns. They'll also be a short film screening 
and the chance for a picnic. If you are interested in knowing more about organisations/campaigns in Greater Manchester,
please come along and join in with the discussions.

> [XR: Connecting With Other Social Campaigns](https://www.eventbrite.com.au/e/extinction-rebellion-connecting-with-other-social-campaigns-tickets-65135433836)
>
> Sunday 28th July, 1:00pm-4:00pm, 
>
> University of Manchester Student Union Theatre Space.

---

### Work That Reconnects – an XR Regenerative Culture Event
![Work That Reconnects – an XR Regenerative Culture Event]({{ site.url }}{{ site.baseurl }}/assets/images/newsletters/20190717/waiting-in-the-wings-work-that-reconnects.jpg)

Join XR Calderdale as they move together through a simple spiral of The Work That Reconnects; Joanna Macy's powerful 
experiential group process. Drawing on deep ecology, systems thinking, and nondualistic spiritual traditions, this 
taster workshop offers an opportunity to give voice to our concerns for our world. The spiral process unblocks feedback
loops within us; allowing us to awaken to the powers within essential for the healing of our world. It builds 
motivation, creativity and courage in these times. This work, developed by Joanna Macy and others over several 
decades of environmental and social activism is at the heart of Extinction Rebellion's regenerative culture.

> [Work That Reconnects – an XR Regenerative Culture Event](https://www.facebook.com/events/452361082226023/)
>
> Sunday 21st July, 11:00am-4:00pm, 
>
> The Nutclough Tavern, Hebden Bridge, HX7 8HA

---

### XR Monday Meeting: Police Tactics in Public Order Situations
![XR Monday Meeting: Police Tactics in Public Order Situations]({{ site.url }}{{ site.baseurl }}/assets/images/newsletters/20190717/waiting-in-the-wings-extinction-rebellion-manchester.jpg)

The Regen group has prepared a training session on violent police tactics during peaceful climate protests in the past 
to raise awareness of risks and how we can best protect each other during protests. Everyone welcome!

> [XR Monday Meeting: Police Tactics in Public Order Situations](https://www.facebook.com/events/344293986263519/)
>
> Monday 22nd July, 6:30pm-8:00pm, 
>
> The Bread Shed Manchester, Grosvenor St, Manchester, M1 7HL

---

### XR Workshop for People Considering First Time Arrest
![XR Workshop for People Considering First Time Arrest]({{ site.url }}{{ site.baseurl }}/assets/images/newsletters/20190717/waiting-in-the-wings-arrestee-support.jpg)

We’re running another training event to better inform members about the risk of arrest. These sessions will cover 
encounters with the police, what happens when you're arrested, the court process, and the possible consequences of 
getting convicted. XR MCR holds the safety of our members of the utmost importance. We strive to be responsible in 
encouraging people to consider arrest and make an informed choice. If you’re thinking of getting arrested then this 
event is essential.


> [XR Workshop for People Considering First Time Arrest](https://www.eventbrite.co.uk/e/xr-workshop-for-people-considering-first-time-arrest-tickets-63963148499)
>
> Thursday 18th July, 6:00pm-9:00pm, 
>
> Friends' Meeting House, 6 Mount Street, Manchester, M2 5NS

---

### XR Local Groups
![XR Local Groups]({{ site.url }}{{ site.baseurl }}/assets/images/newsletters/20190717/waiting-in-the-wings-heading-for-extinction.jpg)

#### Chorlton Extinction Rebellion

In this public talk, speakers will share the latest Climate Science and evidence of ecological breakdown, discussing
where our planet is heading and what, together, we can do about it.

Everyone is welcome. There will be plenty of time to ask questions and you'll hear about how to join the extinction 
rebellion movement.

> Heading for Extinction, and what to do about it
>
> Wednesday 17th July, 7:30pm 
>
> Yoga Café, 286 Barlow Moor Rd, Manchester, M21 8HA

---

#### Didsbury Extinction Rebellion

XR Didsbury is planning an outreach event this Saturday in Didsbury High Street from 9:00am-1:00pm. 

> [XR Didsbury Outreach Event](https://www.facebook.com/groups/661611917602425/events/)
>
> Saturday 20th July, 9:00am-1:00pm 
>
> Didbury High Street

---

#### Macclesfield

We also welcome XR Macclesfield to the fold. 

> XR Macclesfield
>
> Wednesdays from 7:00pm 
>
> The Old Sawmill, Elizabeth Street, Macclesfield

---

#### XR Stockport

XR Stockport are moving their meetings from Wednesdays to alternate Tuesdays.

> XR Macclesfield
>
> Alternate Tuesdays 

---

## Upcoming Events

Check out our [events](/events "XRMCR Events") page for details of upcoming events.

## Would you like to write for this newsletter?

Send us an email at [hello@xrmcr.org](mailto:hello@xrmcr.org "Email xrmcr").

> There’s no Pride in Ecocide.
