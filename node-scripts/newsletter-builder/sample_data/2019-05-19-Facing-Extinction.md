---
layout: single
author: stockysatlarge
title: Facing Extinction
author_profile: true
categories: []
tags: []
share: true
---

This blog article by Catherine Ingram was shared by Claire Stocks. It is very long but it details a lot of what we are facing and why it is hard to accept what is happening: [Facing Extinction](http://www.catherineingram.com/facingextinction/ "Facing Extinction).

Some quotes that stuck out to me:

> They are as children, playing with their toys in a house on fire.
> — Gautama Buddha

As you begin to awaken to the spectre of extinction, you will likely feel the powerful lure of your usual distractions. You may want to go back to sleep. But denial will become harder and harder to maintain because once your attention has turned to this subject, you will see the evidence of it everywhere, both locally and globally.

And you will find yourself among the throngs of humanity who are easily distracted and amused, playing with their toys as the house burns, “tranquilized by the trivial”

The Great Filter proposes that before a civilization reaches the level of development that would allow for intergalactic communication and travel, it wipes itself out through climate change, overpopulation, or other factors having to do with the rise of technological civilization… The theory of The Great Filter allowed me to see that humans are just doing what we were evolutionarily destined to do. It is not an aberration of evolution, even though it will destroy all complex life. Nor is it is the result of any one thread of evolution, any particular age or technological advancement or economic system
