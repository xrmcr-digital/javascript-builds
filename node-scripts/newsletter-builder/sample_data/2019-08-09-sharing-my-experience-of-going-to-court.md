---
layout: single
author: zoe
title: \"I was arrested in Parliament Square\" - an XRMCR Rebel\'s honest account of going to court in London
author_profile: true
categories: [nvda, arrest]
tags: []
share: true
excerpt: "Zoe shares her experience of going to court after her arrest at the XR climate change protests in April 2019"
---

**Given that there are so many lovely rebel colleagues going to court, waiting to go to court, and potentially going to court in the future, I wanted to share my experience in case it helps others.**

## Taking responsibility

I was arrested in Parliament Square in April and very much took responsibility for my actions. When the various letters and paperwork arrived in July, I was nonetheless nervous (being my first arrest). However, talking to fellow rebels from my affinity group who were a little further along the process was helpful, as was talking it through with a very help helpful solicitor.

I had decided that I wanted to plead ‘not guilty’ and go through with the trial process, but I found the anticipation of the week or so leading up to the plea hearing pretty stressful.

## After the plea hearing

> ...it was largely a lovely experience!

A week on from the plea hearing I can honestly say that the anticipation was much worse than the actual experience. I am sure that the system is trying to prosecute us all for their reasons, but unintentionally I think they have added to the movement building effect.

On the day, I got up at 5am to get the train to be in central London by 9am in time for my ‘9:30’ slot… as it was I didn’t go into the court room itself until 2:30pm – thankfully my solicitor had warned me of the waiting around risk, so I’d brought a drink and food...

But from the moment I arrived at the City of London Magistrates court it was largely a lovely experience! Because they have ‘batched’ our hearings together in large numbers on set days, it meant that there were lots of lovely XR folk to talk to. An opportunity to meet new people, share stories, as well as see some familiar faces.

The Arrestee Support was amazing – from the care and attention shown, to the offers of practical help, and the snacks and fruit supplied. I couldn’t have wished for more. Love and solidarity abounded.

And there were rebels there from the XR Media team too getting people’s stories and taking photos to help get stories out, even when this isn’t new news to the media anymore.

![City of London Court]({{ site.url }}{{ site.baseurl }}/assets/images/city-of-london-court.jpg "City of London Court")

## Deciding to go public

On the way down on the train I also finally decided, after some wavering, to go public about my arrest and plea hearing on all my social media, including LinkedIn. I am so glad I did, as I received so much amazing support, even from unexpected quarters.

So, will I be nervous going to trial, yes probably, but that’s just me. But I will be a lot less nervous now I know what to expect and knowing there is so much support from so many people for what I, and we, are all doing.

Roll on 7th October…

> Love, rage and rebellion

Zoe xxx
