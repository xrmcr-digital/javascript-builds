---
layout: single
author: stockysatlarge
title: How to get involved in Extinction Rebellion Manchester
author_profile: true
categories: [info]
tags: []
share: true
permalink: /how-to-get-involved-in-extinction-rebellion-manchester
---

To help you understand where to start especially if you're new to Extinction Rebellion, here’s a guide to different levels of involvement depending on how engaged you would like to be.

To contact a Working Group directly, check out our [contact page](/contact "Contact Extinction Rebellion Manchester").

### Starting out

> ‘I’m new and want to find out more about Extinction Rebellion and what's happening in my area’

Join Extinction Rebellion by [signing up here](/signup "Sign up") and for any / or all of our media channels.

This will enable you to stay in touch with news and details of events we are running - from actions to outreach and fundraisers.

You should also attend one of our talks - **‘Climate Change: heading for extinction and what to do about it’.**

(You can watch a video on YouTube - [this original version](https://www.youtube.com/watch?v=b2VkC4SnwY0&list=PLnzA40Blbb2kiECebMMSPOjg8pBGdnf2A&index=17&t=104s "Youtube video") (since updated) given by one of XR's founders, Gail Bradbrook, helped spark XR's growth in summer 2018).

This sets out the science of our climate crisis as well as the philosophy of direct action and explains XR’s tactics and demands.

You may also want to know more about direct action - so we offer regular talks in Greater Manchester on **‘Non Violent Direct Action’** (NVDA) and what's involved.

All talks are advertised on our website, on our [Facebook page](https://www.facebook.com/xrmcr/ "xrmcr Facebook") and via our newsletter.

We hold regular meetings in central Manchester - check our events listings on our website or our Facebook page - and new Local Groups are setting up all the time - [check our map](https://www.google.com/maps/d/u/0/viewer?mid=11jUqqjTHMThksd4KbvGGzb3I3Cr3PkBl&ll=26.57676882391495%2C-7.557160111745816&z=-1 "Google Maps").

We are entirely run by volunteers giving up their spare time.

### Getting More Involved

![Manchester Protest]({{ site.url }}{{ site.baseurl }}/assets/images/mcr-protest.jpg "Protesting in Manchester")

The first step is to join a Local Group!

In Manchester until now we've only had one group, XR MCR, but International Rebellion Week triggered a huge growth in interest so there are new groups springing up across the region now - such as Salford, Wigan, Trafford, Stockport, and Bury.

We're helping local groups to form in a few different ways:-

- Face to face: For the time being we're running meetings every Monday in central Manchester at 6pm at the Breadshed . Come along and we will help you meet other people from your area and fill you in on what's happening and how you can get involved.

- Keep in touch - Sign up for our mailing list (as per step 1) - it'll take a bit of time to sort things out so bear with us - the newsletter is the best way to stay in touch and make sure you hear the latest info and announcements - as well as bookmarking our website and following our Facebook page.

For many people a local group is where interest in XR will be best focused - working with other people in your local area to plan actions and outreach that are relevant to you and your local community.

Anyone can take action in the name of XR - you just need to [sign up to the 10 principles](https://rebellion.earth/the-truth/about-us/ "10 principles") especially non-violence, and focus on [XR’s 3 demands](https://rebellion.earth/the-truth/about-us/ "3 Demands"), and find a couple of other people who agree with you to act with you.

However it is clear from feedback people also want to feel part of a wider Manchester XR collective.

### Getting Really Stuck In

> ‘I want to take my interest further and can commit some free time / would like to help by applying my skills or learning new ones’

Right now XR MCR is growing its Working Groups in order to support the movement as it expands in Manchester.

We have the following working groups coordinating activities and are actively looking for people who can get involved and help really grow XR in Manchester.

### Actions

### Admin

### Creative Arts

### Platforms ie tech

### Media & Messaging

### Outreach and Training

### Regenerative Culture and Wellbeing

We are looking for volunteers for every working group - people who are able to give some regular time and commit to regular meetings (online or face to face) and take on tasks - its a great way to meet new friends and learn new skills.

We organise work via a tool called Slack, where there are lots of other channels for smaller topics of interest.

If you are not able to commit to a Working Group, there are always lots of small jobs that need doing! These are posted via the #jobs channel.

### Going Further!

![Die In]({{ site.url }}{{ site.baseurl }}/assets/images/die-in.jpg "Die in at St. Peter's Square")

> “I want to set up or help run a Local Group, Working Group/ or a Working Sub-Group”

Right now, if you have some time to give and skills to share, we’d encourage you to start by joining a Working Group.

However, we also know people are looking for more Local Groups in Manchester, so they can join in near where they live.

So if there isn’t a local group in your area and you are up for doing more - you might like to consider finding other like minds and starting one yourself.

XR MCR will help support Local Groups as they get up and running.

For instance, you could organise a public meeting where XR will book speakers to come and deliver the introductory talk, or host Non Violent Direction Action training. You can request this by emailing eventsxr@gmail{dot}com.

### How is XR Organised Here?

Here is a simple diagram which will undoubtedly quickly evolve - this is just how we are approaching it for now.

It shows the three main types of groups that need to fit together: Regional (XR MCR), Local (eg Wigan) and Working groups (eg Actions).

![XRMCR Structure]({{ site.url }}{{ site.baseurl }}/assets/images/xrmcr-structure.jpg "XRMCR Structure")

#### Regional group (XR MCR)

The role of this group is to help establish a flourishing XR in Greater Manchester; to support the expansion of more local groups; to organise impactful regional-scale action through well-functioning working groups. Although called XR MCR, the group is supporting activity across Greater Manchester. As more local groups grow up across GM, we expect this will change.

#### Working Groups (eg Media or Outreach)

XR operates on the principle of ‘self organising’ decentralised teams. Each working group operates autonomously and organises it’s own remit and activities. This means groups don’t have to seek approval from anywhere to do stuff within this remit.

Each working group has two coordinators - they're not leaders or spokespeople but there to keep the group moving forward and running in accordance with XR’s principles including making sure meetings are facilitated to be inclusive & minutes are circulated.

An ideal number for a working group is about 8-15 people who can offer time to take the group forward - exact numbers depends on the remit of the group and how much time each person can give.

The more people in a Working Group, the more work can get shared around - but once groups get too big to have one conversation with everyone involved, they're encouraged to form Sub-Groups, which is how they handle specific themed tasks (eg the Media Working Group may form a mini Sub Group for ‘Internal Communications’).

#### I've heard about Affinity Groups - how do they fit in?

Affinity Groups are support groups of 8-12 people that look after each other before , during and after actions. They are not part of the above structure.

### Glossary

**A Local Group (LG)** is a group of people that gather to create an Extinction Rebellion (XR) community presence in a local area by building support and taking action towards XR’s demands. They are similar to traditional forms of community organising but a crucial difference is they decentralise into Working Groups as much as possible.

**A Working Group (WG)** focuses on a specific area of work and is allowed to decide how that work gets done. For example, if the Outreach team want to start outreaching to a certain community, they can decide it in their group without asking permission from the rest of the local or regional group.

**A Coordination Group (CG)** is a group of the coordinators from the different WGs which meet to decide key issues and how the working groups will be strategically focussed.

**Affinity Groups (AG)** are direct action support groups. They are made up of 8-12 people and are autonomous to do the actions they want to in the name of XR as long as they adhere to the Principles and Values. They’re there to create an effective and supportive crew for actions and usually become friends too, who meet to support each other on an ongoing basis, not just at actions.

**A Regional Group (RG)** is a group that is supporting the growth of XR in a particular region eg XR MCR is supporting growth in Greater Manchester. It may become defunct once Local Groups are fully established, or it may continue helping link Local Groups and support regional activity.

![XRMCR in London]({{ site.url }}{{ site.baseurl }}/assets/images/xrmcr-in-london.jpg "XRMCR in London")
