#!/usr/bin/env node

import fs from 'fs';
import args from 'args';
import rp from 'request-promise';
import * as path from 'path';

args
    .option('keyfile', 'Path to json facebook key file')
    .option('warndays', 'Days left before a warning is issued', '10');

const flags = args.parse(process.argv);
const keys = JSON.parse(fs.readFileSync(flags.keyfile).toString());
const WARN_DAYS = parseInt(flags.warndays, 10);

const now = new Date().getTime();

function check_expiry(name : string, result : any) : number {
  console.log(`Checking key for: ${name}`);
  if (result.data_access_expires_at === 0) {
    console.error('Key has expired!!!');
    return 1;
  }
  const expiryDate = new Date(result.data_access_expires_at * 1000);
  const daysToExpiry = Math.round((expiryDate.getTime() - now) / (1000 * 60 * 60 * 24));
  console.log(`Expires: ${expiryDate} (${daysToExpiry} days)`);
  if (daysToExpiry < WARN_DAYS) {
    console.error(`Key will expire in ${WARN_DAYS} days or less!`);
  }
  return 0;
}

const promises: Promise<number>[] = [];
for (const key of keys) {
  const options = {
    uri: 'https://graph.facebook.com/v4.0/debug_token',
    qs: {
      input_token: key.access_token,
      access_token: key.access_token,
    },
  };
  promises.push(new Promise<number>((resolve, reject) => {
    rp(options).
        then((resp) => {
          const d = JSON.parse(resp).data;
          resolve(check_expiry(key.name, d));
        });
  }));
}

Promise.all(promises)
  .then((results) => {
    process.exit(results.reduce((a, b) => a + b, 0));
  });
