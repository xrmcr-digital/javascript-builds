# Events Hub

# Start
`npm run start`
This will run the dev built version of the events hub.
Events will be output to `./dist/events.json`

## Dev
`npm run serve`
This will start a webpack watcher, which will recompile at on file change.
Output will be `./dist/main.js`.

## Build
`npm run build`
This will build a production version of the events hub.
Output will be `../../dist/events-hub`.

## lint
`npm run lint`
This will run TSLint running a mildly relaxed version of the AirBnb TSLint rules.
