import fetch from 'node-fetch';

import { IFacebookPage } from './interfaces';
import { IEvent } from '../interfaces';

export class FBClient {

  constructor(private pageId: string, private accessToken: string, private name: string) {
  }

  async getPage(): Promise<IEvent[]> {

    const path = `https://graph.facebook.com/v4.0/${this.pageId}`;

    const eventsFields = [
      'cover',
      'description',
      'start_time',
      'end_time',
      'name',
      'place',
      'is_cancelled',
      'maybe_count',
      'interested_count',
      'attending_count',
    ].join(',');

    const fields = [
      'name',
      'id',
      `events.limit(1000){${eventsFields}}`,
    ];

    const url = new URL(path);
    url.searchParams.append('access_token', this.accessToken);
    url.searchParams.append('fields', fields.join(','));
    url.searchParams.append('until', '+ 3 months');
    url.searchParams.append('include_canceled', 'true');

    return await fetch(url.href)
      .then(res => res.json())
      .then((res) => {
        console.log(`Parsing ${this.name}`);
        return res;
      })
      .then(res => this.parser(res));
  }

  parser = (page: IFacebookPage): IEvent[] => page.events.data.map((event) => {

    const location: any = {
      asArray: [],
    };

    if (event.place) {
      location.name = event.place.name || undefined;
      location.asArray = [event.place.name];
    }

    if (event.place && event.place.location) {
      location.street = event.place.location.street || undefined;
      location.city = event.place.location.city || undefined;
      location.country = event.place.location.country || undefined;
      location.zip = event.place.location.zip || undefined;
      location.latitude = event.place.location.latitude || undefined;
      location.longitude = event.place.location.longitude || undefined;
      location.asArray = [
        ...location.asArray,
        location.street,
        location.city,
        location.country,
        location.zip,
      ];
    }

    location.asArray = location.asArray.filter((i: string) => Boolean(i));

    return {
      location,
      hosted_by: {
        id: page.id,
        name: page.name,
        url: `https://www.facebook.com/${page.id}/`,
      },
      description: event.description,
      title: event.name,
      startTime: event.start_time,
      endTime: event.end_time,
      fbid: event.id,
      url: `https://www.facebook.com/events/${event.id}/`,
      image: event.cover && event.cover.source ? event.cover.source : undefined,
      is_cancelled: event.is_cancelled,
      interested_count: event.interested_count,
      maybe_count: event.maybe_count,
      attending_count: event.attending_count,
    };

  })

}
