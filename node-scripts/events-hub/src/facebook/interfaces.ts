export interface IRawFacebookPage {
  id: string;
  access_token: string;
  name: string;
  disabled?: boolean;
}

export interface IRawFacebookEvent {
  cover: {
    offset_x: number;
    offset_y: number;
    source: string;
    id: string;
  };
  description: string;
  start_time: string;
  end_time: string;
  place: {
    name: string;
    location: {
      city: string;
      country: string;
      latitude: number;
      longitude: number;
      street: string;
      zip: string;
    },
    id: string;
  };
  maybe_count: number;
  interested_count: number;
  attending_count: number;
  is_cancelled: boolean;
  id: string;
  name: string;
}

export interface IFacebookPage {
  id: string;
  name: string;
  events: {
    data: IRawFacebookEvent[];
  };
}
