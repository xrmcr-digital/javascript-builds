#!/usr/bin/env node

import fs from 'fs';
import args from 'args';
import _ from 'lodash';

import { FBClient } from './facebook/client';
import { IEvent } from './interfaces';
import * as path from 'path';
import { IRawFacebookPage } from './facebook/interfaces';

args
  .option('pages', 'Path to Pages JSON', './pages.json')
  .option('path', 'Path to output events', './')
  .option('filename', 'Events filename', 'events.json');

const flags = args.parse(process.argv);

const { pages } = flags;

const filePath = path.resolve(process.cwd(), pages);
const fbPages = JSON.parse(fs.readFileSync(filePath).toString());

const fbEvents = fbPages
  .filter((page: IRawFacebookPage) => !page.disabled)
  .map((page: IRawFacebookPage) => new FBClient(page.id, page.access_token, page.name))
  .map(async (page: FBClient) => await page.getPage());

const events: IEvent[] = [
  ...fbEvents,
];

const eventsSorter = (a: IEvent, b: IEvent) => {
  const aStart = Number(new Date(a.startTime));
  const bStart = Number(new Date(b.startTime));
  return (aStart > bStart) ? 1 : -1;
};

const eventsFilter = (event: IEvent): boolean => {
  const now = new Date();
  now.setHours(0, 0, 0);
  const eventEndDate = new Date(event.startTime);
  eventEndDate.setHours(23, 59, 59);

  return Number(now) < Number(eventEndDate);
};

const eventsUniqBy = (event: IEvent): string => {
  // Cant do deep uniquing
  return `${event.title}-${event.startTime}-${event.startTime}`;
};

Promise.all(events)
  .then(events => _.flattenDeep(events))
  .then(events => _.uniqBy(events, eventsUniqBy))
  .then(events => events.filter(eventsFilter))
  .then(events => events.sort(eventsSorter))
  .then(events => JSON.stringify(events, null, 2))
  .then(data => fs.writeFileSync(`${flags.path}/${flags.filename}`, data));
