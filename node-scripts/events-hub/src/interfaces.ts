export interface IEvent {
  hosted_by: {
    id: string;
    name: string;
    url: string;
  };
  description: string;
  title: string;
  image?: string;
  interested_count: number;
  maybe_count: number;
  attending_count: number;
  is_cancelled: boolean;
  location: {
    name: string;
    street?: string;
    city?: string;
    country?: string;
    zip?: string;
    latitude?: number;
    longitude?: number;
    asArray: any[]
  };
  startTime: string;
  endTime: string;
  fbid?: string;
  url?: string;
}
