import fs from 'fs';
import dotenv from 'dotenv';
import path from 'path';
import args from 'args';
import gmailSend from 'gmail-send';

args
  .option('html', 'HTML contents for the email')
  .option('to', 'Address to send the email to')
  .option('subject', 'Email Subject', 'XRMCR Newsletter');

const flags = args.parse(process.argv);
const { subject, to, html } = flags;

let GMAIL_ACCOUNT;
let GMAIL_APP_PASS;

try {
  GMAIL_ACCOUNT = dotenv.config().parsed.GMAIL_ACCOUNT;
  GMAIL_APP_PASS = dotenv.config().parsed.GMAIL_APP_PASS;
} catch (e) {
  GMAIL_ACCOUNT = process.env.GMAIL_ACCOUNT;
  GMAIL_APP_PASS = process.env.GMAIL_APP_PASS;
}

if (!to) {
  console.error('ERROR: To is Required\n');
  process.exit(1);
}

if (!html) {
  console.error('ERROR: HTML is Required\n');
  process.exit(1);
}

if (!GMAIL_ACCOUNT) {
  console.error('ERROR: ENV GMAIL_ACCOUNT is Required\n');
  process.exit(1);
}

if (!GMAIL_APP_PASS) {
  console.error('ERROR: ENV GMAIL_APP_PASS is Required\n');
  process.exit(1);
}

const filePath = path.resolve(process.cwd(), html);
const newsletter = fs.readFileSync(filePath).toString();

const options = {
  to,
  subject,
  user: GMAIL_ACCOUNT,
  pass: GMAIL_APP_PASS,
  html: newsletter,
};

const send = gmailSend(options);

send()
  .then(() => {
    console.log('Email Sent');
    process.exit(0);
  })
  .catch((e: Error) => {
    console.log('Email Failed');
    console.log(e);
    process.exit(1);
  });
