declare module "gmail-send" {
  const send: (options: any) => () => Promise<any>;
  export default send;
}
